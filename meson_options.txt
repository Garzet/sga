option('tune',
       type: 'boolean',
       value: false,
       description: 'Build hyperparamter tuning facilities.')

option('examples',
       type: 'boolean',
       value: false,
       description: 'Build examples.')

option('tests',
       type: 'boolean',
       value: false,
       description: 'Build tests.')
