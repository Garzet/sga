#include <catch2/catch_test_macros.hpp>

#include "SHO/Solution.hpp"

TEST_CASE("PermutationVectorCycleCrossover")
{
    using namespace SHO::PermutationVector;
    CycleCrossover crossover;

    {
        const Solution first_parent  = {1, 2, 3, 4, 5, 6, 7, 8};
        const Solution second_parent = {3, 8, 7, 2, 6, 5, 1, 4};
        REQUIRE(crossover.mate(first_parent, second_parent) ==
                Solution({1, 8, 3, 2, 5, 6, 7, 4}));
    }
    {
        const Solution p1 = {7, 4, 1, 8, 5, 6, 2, 3};
        const Solution p2 = {3, 2, 1, 4, 5, 6, 8, 7};
        REQUIRE(crossover.mate(p1, p2) == Solution({7, 2, 1, 4, 5, 6, 8, 3}));
    }
    {
        const Solution first_parent  = {4, 2, 5, 1, 3};
        const Solution second_parent = {2, 3, 1, 5, 4};
        REQUIRE(crossover.mate(first_parent, second_parent) ==
                Solution({4, 2, 1, 5, 3}));
    }
}

TEST_CASE("PermutationVectorPartiallyMappedCrossover")
{
    using namespace SHO::PermutationVector;
    PartiallyMappedCrossover crossover;

    {
        const Solution first_parent  = {1, 6, 9, 5, 7, 3, 2, 8, 4};
        const Solution second_parent = {9, 2, 8, 3, 7, 1, 4, 6, 5};
        REQUIRE(crossover.mate(first_parent, second_parent, {2, 6}) ==
                Solution({8, 2, 9, 5, 7, 3, 4, 6, 1}));

        // All data from the first parent.
        REQUIRE(crossover.mate(first_parent, second_parent, {0, 9}) ==
                first_parent);

        // All data from the second parent.
        REQUIRE(crossover.mate(first_parent, second_parent, {3, 3}) ==
                second_parent);
    }
    {
        const Solution first_parent  = {2, 1, 3, 7, 4, 6, 5, 8};
        const Solution second_parent = {1, 2, 8, 7, 3, 4, 6, 5};
        REQUIRE(crossover.mate(first_parent, second_parent, {0, 5}) ==
                Solution({2, 1, 3, 7, 4, 8, 6, 5}));
        REQUIRE(crossover.mate(first_parent, second_parent, {4, 8}) ==
                Solution({1, 2, 3, 7, 4, 6, 5, 8}));
        REQUIRE(crossover.mate(first_parent, second_parent, {5, 6}) ==
                Solution({1, 2, 8, 7, 3, 6, 4, 5}));
    }
    {
        const Solution first_parent  = {4, 2, 5, 1, 3};
        const Solution second_parent = {2, 3, 1, 5, 4};
        REQUIRE(crossover.mate(first_parent, second_parent, {1, 4}) ==
                Solution({3, 2, 5, 1, 4}));
        REQUIRE(crossover.mate(first_parent, second_parent, {0, 2}) ==
                Solution({4, 2, 1, 5, 3}));
    }
}

TEST_CASE("PermutationVectorOrderCrossover")
{
    using namespace SHO::PermutationVector;
    OrderCrossover crossover;

    {
        const Solution first_parent  = {1, 2, 3, 4, 5, 6, 7, 8};
        const Solution second_parent = {3, 8, 7, 2, 6, 5, 1, 4};
        REQUIRE(crossover.mate(first_parent, second_parent, {3, 6}) ==
                Solution({1, 3, 8, 4, 5, 6, 7, 2}));
        REQUIRE(crossover.mate(first_parent, second_parent, {0, 4}) ==
                Solution({1, 2, 3, 4, 6, 5, 8, 7}));
        REQUIRE(crossover.mate(first_parent, second_parent, {5, 8}) ==
                Solution({3, 2, 5, 1, 4, 6, 7, 8}));

        // All data from the first parent.
        REQUIRE(crossover.mate(first_parent, second_parent, {0, 8}) ==
                first_parent);

        // All data from the second parent.
        REQUIRE(crossover.mate(first_parent, second_parent, {3, 3}) ==
                second_parent);
    }
    {
        const Solution first_parent  = {4, 2, 5, 1, 3};
        const Solution second_parent = {2, 3, 1, 5, 4};
        REQUIRE(crossover.mate(first_parent, second_parent, {1, 4}) ==
                Solution({4, 2, 5, 1, 3}));
        REQUIRE(crossover.mate(first_parent, second_parent, {0, 2}) ==
                Solution({4, 2, 1, 5, 3}));
    }
}
