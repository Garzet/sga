#include <catch2/catch_test_macros.hpp>

#include "SHO/SO/ExhaustiveSearch.hpp"

TEST_CASE("ExhaustiveSearch")
{
    struct Advancement final : public SHO::Advancement<int> {
        [[nodiscard]] virtual bool operator()(int& solution)
        {
            ++solution;
            return true;
        }
    };
    struct Evaluator final : public SHO::SO::Evaluator<int> {
        [[nodiscard]] double operator()(const int& solution) final { return solution; }
    };

    SHO::MaxRunningTime max_running_time{std::chrono::seconds{3}};

    Advancement advancement;
    Evaluator evaluator;
    const auto [met_criterion, individual, state] =
        SHO::SO::ExhaustiveSearch<int>::run(evaluator, {max_running_time}, advancement, 20);
}
