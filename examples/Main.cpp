#include <iostream>
#include <array>
#include <sstream>
#include <algorithm>
#include <unordered_map>

#include "Problem.hpp"

#include <SHO/SO/SSGA.hpp>
#include <SHO/SO/ExhaustiveSearch.hpp>
#include <SHO/SO/LocalSearch.hpp>
#include <SHO/MO/NSGA2.hpp>
#include <SHO/MO/ExhaustiveSearch.hpp>

namespace {

std::string get_usage_string(std::string_view executable_name) noexcept;
std::string get_available_examples() noexcept;

void ssga_square();
void ssga_onemax();
void ssga_tsp();
void nsga2_schaffer();
void exhaustivesearchso_square();
void exhaustivesearchmo_schaffer();
void localsearch_square();
void localsearch_tsp();

using ExampleFn = void (*)();

const std::unordered_map<std::string_view, ExampleFn> examples = {
    {"ssga_square", ssga_square},
    {"ssga_onemax", ssga_onemax},
    {"ssga_tsp", ssga_tsp},
    {"nsga2_schaffer", nsga2_schaffer},
    {"exhaustivesearchso_square", exhaustivesearchso_square},
    {"exhaustivesearchmo_schaffer", exhaustivesearchmo_schaffer},
    {"localsearch_square", localsearch_square},
    {"localsearch_tsp", localsearch_tsp},
};

}

int main(int argc, char** argv)
{
    const std::string_view executable_name = argv[0];

    if (argc < 2) {
        std::cout << "No arguments have been provided.\n"
                  << get_usage_string(executable_name);
        return EXIT_FAILURE;
    }
    if (argc > 2) {
        std::cout << "Warning: multiple arguments provided, only \"" << argv[1]
                  << "\" is used.\n";
    }

    const std::string_view example = argv[1];
    const auto it                  = examples.find(example);
    if (it == examples.end()) {
        std::cout << "Invalid example \"" << example << "\". "
                  << get_available_examples();
        return EXIT_FAILURE;
    }
    const auto& example_funtion = it->second;
    std::cout << "Running example \"" << example << "\".\n";
    example_funtion();
}

namespace {

std::string get_usage_string(std::string_view executable_name) noexcept
{
    std::ostringstream oss;
    oss << "Usage: " << executable_name << " example\n"
        << get_available_examples();
    return oss.str();
}

std::string get_available_examples() noexcept
{
    std::ostringstream oss;
    oss << "Available examples:\n";
    for (const auto& [example, function] : examples) {
        oss << '\t' << example << '\n';
    }
    return oss.str();
}

void ssga_square()
{
    using Algorithm  = SHO::SO::SSGA<SHO::FloatingPoint::Solution>;
    using Population = Algorithm::Population;
    using State      = Algorithm::State;

    const auto& domain                           = Problem::Square::domain;
    constexpr static std::size_t population_size = 10000;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(7)));
    terminations.push_back(std::make_unique<SHO::MaxEvaluations>(50000));

    const std::vector<double> crossover_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::Crossover>> crossovers;
    crossovers.push_back(
        std::make_unique<SHO::FloatingPoint::ArithmeticCrossover>());

    const std::vector<double> selection_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::Selection>> selections;
    selections.push_back(
        std::make_unique<Algorithm::TournamentSelection>(population_size, 50));

    const std::vector<double> mutation_weights = {0.2, 0.8};
    std::vector<std::unique_ptr<Algorithm::Mutation>> mutations;
    mutations.push_back(std::make_unique<SHO::FloatingPoint::RandomMutation>(
        domain.min, domain.max));
    mutations.push_back(
        std::make_unique<SHO::FloatingPoint::GaussianMutation>(50.0));

    std::uniform_real_distribution<double> dist{domain.min, domain.max};
    std::vector<double> initial_solutions;
    initial_solutions.reserve(population_size);
    for (std::size_t i = 0; i < population_size; i++) {
        initial_solutions.push_back(dist(SHO::Random::get_generator()));
    }

    Algorithm soga(std::make_unique<Problem::Square::Evaluator>(),
                   std::move(terminations),
                   {std::move(selections), selection_weights},
                   {std::move(crossovers), crossover_weights},
                   {std::move(mutations), mutation_weights},
                   0.02);

    std::cout << fmt::format(
        "{:^10} {:^15} {:^15}\n", "Iteration", "Solution", "Fitness");
    const auto& [final_population, met_criterion, state] = soga.run(
        std::move(initial_solutions),
        [](const Population& population, const State& state) {
            static constexpr unsigned long long log_frequency = 2500;
            if (state.iteration % log_frequency == 0) {
                auto it = population.find_best_individual();
                std::cout << fmt::format("{:>7}       {:> .3e}      {:> .3e}\n",
                                         state.iteration,
                                         it->solution,
                                         it->get_fitness());
            }
        });

    std::cout << met_criterion.get_termination_message()
              << "\nNumber of iterations: " << state.iteration
              << "\nBest solution: " << final_population.front().solution
              << "\nBest fitness:  " << final_population.front().get_fitness()
              << '\n';
}

void ssga_onemax()
{
    using Algorithm  = SHO::SO::SSGA<SHO::BinaryVector::Solution>;
    using Population = Algorithm::Population;
    using State      = Algorithm::State;

    constexpr static std::size_t population_size = 20;
    constexpr static std::size_t vector_size     = 100;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(7)));
    terminations.push_back(std::make_unique<SHO::MaxEvaluations>(30000));

    const std::vector<double> crossover_weights = {0.5, 0.5};
    std::vector<std::unique_ptr<Algorithm::Crossover>> crossovers;
    crossovers.push_back(
        std::make_unique<SHO::BinaryVector::SinglePointCrossover>());
    crossovers.push_back(
        std::make_unique<SHO::BinaryVector::UniformCrossover>());

    const std::vector<double> selection_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::Selection>> selections;
    selections.push_back(
        std::make_unique<Algorithm::TournamentSelection>(population_size, 5));

    const std::vector<double> mutation_weights = {0.2, 0.8};
    std::vector<std::unique_ptr<Algorithm::Mutation>> mutations;
    mutations.push_back(std::make_unique<SHO::BinaryVector::RandomMutation>());
    mutations.push_back(
        std::make_unique<SHO::BinaryVector::RandomFlipMutation>());

    std::vector<SHO::BinaryVector::Solution> initial_solutions(
        population_size, SHO::BinaryVector::Solution(vector_size, false));

    Algorithm soga(std::make_unique<Problem::OneMax::Evaluator>(),
                   std::move(terminations),
                   {std::move(selections), selection_weights},
                   {std::move(crossovers), crossover_weights},
                   {std::move(mutations), mutation_weights},
                   0.02);

    std::cout << fmt::format("{:^10} {:^15}\n", "Iteration", "Fitness");
    const auto& [final_population, met_criterion, state] =
        soga.run(std::move(initial_solutions),
                 [](const Population& population, const State& state) {
                     static constexpr unsigned long long log_frequency = 2500;
                     if (state.iteration % log_frequency == 0) {
                         const auto it = population.find_best_individual();
                         std::cout << fmt::format("{:>7}           {}\n",
                                                  state.iteration,
                                                  it->get_fitness());
                     }
                 });

    std::cout << met_criterion.get_termination_message()
              << "\nNumber of iterations: " << state.iteration
              << "\nBest solution: " << final_population.front().solution
              << "\nBest fitness:  " << final_population.front().get_fitness()
              << '\n';
}

void ssga_tsp()
{
    using Algorithm  = SHO::SO::SSGA<SHO::PermutationVector::Solution>;
    using Population = Algorithm::Population;
    using State      = Algorithm::State;

    constexpr static std::size_t population_size = 1000;

    auto distance_matrix =
        Problem::TravelingSalesman::DistanceMatrix::load_from_tsplib_file(
            "../TSPLIB/bays29.tsp");

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(4)));

    const std::vector<double> crossover_weights = {0.33, 0.33, 0.33};
    std::vector<std::unique_ptr<Algorithm::Crossover>> crossovers;
    crossovers.push_back(
        std::make_unique<SHO::PermutationVector::CycleCrossover>());
    crossovers.push_back(
        std::make_unique<SHO::PermutationVector::OrderCrossover>());
    crossovers.push_back(
        std::make_unique<SHO::PermutationVector::PartiallyMappedCrossover>());

    const std::vector<double> selection_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::Selection>> selections;
    selections.push_back(
        std::make_unique<Algorithm::TournamentSelection>(population_size, 3));

    const std::vector<double> mutation_weights = {1.0, 1.0};
    std::vector<std::unique_ptr<Algorithm::Mutation>> mutations;
    mutations.push_back(
        std::make_unique<SHO::PermutationVector::SwapMutation>());
    mutations.push_back(
        std::make_unique<SHO::PermutationVector::RandomMutation>());

    Algorithm soga(std::make_unique<Problem::TravelingSalesman::Evaluator>(
                       std::move(distance_matrix)),
                   std::move(terminations),
                   {std::move(selections), selection_weights},
                   {std::move(crossovers), crossover_weights},
                   {std::move(mutations), mutation_weights},
                   0.1);

    std::vector<SHO::PermutationVector::Solution> initial_solutions;
    initial_solutions.reserve(population_size);
    const auto n_nodes = distance_matrix.get_number_of_nodes();
    SHO::PermutationVector::Solution solution;
    solution.reserve(n_nodes);
    for (std::size_t i = 0; i < n_nodes; i++) { solution.push_back(i); }
    for (std::size_t i = 0; i < population_size; i++) {
        std::shuffle(
            solution.begin(), solution.end(), SHO::Random::get_generator());
        initial_solutions.push_back(solution);
    }

    std::cout << fmt::format("{:^14} {:^15}\n", "Iteration", "Fitness");
    const auto& [final_population, met_criterion, state] =
        soga.run(std::move(initial_solutions),
                 [](const Population& population, const State& state) {
                     static constexpr unsigned long long log_frequency = 40000;
                     if (state.iteration % log_frequency == 0) {
                         const auto it = population.find_best_individual();
                         std::cout << fmt::format("{:>10}         {}\n",
                                                  state.iteration,
                                                  it->get_fitness());
                     }
                 });

    std::cout << met_criterion.get_termination_message()
              << "\nNumber of iterations: " << state.iteration
              << "\nBest solution: " << final_population.front().solution
              << "\nBest fitness:  " << final_population.front().get_fitness()
              << '\n';
}

void nsga2_schaffer()
{
    using Algorithm  = SHO::MO::NSGA2<SHO::FloatingPoint::Solution, 2>;
    using Population = Algorithm::Population;
    using State      = Algorithm::State;

    static constexpr std::size_t population_size = 30;

    const auto& domain         = Problem::Schaffer::domain;
    const auto& fitness_domain = Problem::Schaffer::Evaluator::fitness_domain;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(5)));

    const std::vector<double> crossover_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::Crossover>> crossovers;
    crossovers.push_back(
        std::make_unique<SHO::FloatingPoint::ArithmeticCrossover>());

    std::vector<double> selection_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::Selection>> selections;
    selections.push_back(
        std::make_unique<Algorithm::TournamentSelection>(population_size, 5));

    std::vector<double> mutation_weights = {0.2, 0.8};
    std::vector<std::unique_ptr<Algorithm::Mutation>> mutations;
    mutations.push_back(std::make_unique<SHO::FloatingPoint::RandomMutation>(
        domain.min, domain.max));
    mutations.push_back(
        std::make_unique<SHO::FloatingPoint::GaussianMutation>(50.0));

    std::uniform_real_distribution<double> dist(domain.min, domain.max);
    std::vector<double> initial_solutions;
    initial_solutions.reserve(population_size);
    for (std::size_t i = 0; i < population_size; i++) {
        initial_solutions.push_back(dist(SHO::Random::get_generator()));
    }

    Algorithm moga(std::make_unique<Problem::Schaffer::Evaluator>(),
                   std::move(terminations),
                   {std::move(selections), selection_weights},
                   {std::move(crossovers), crossover_weights},
                   {std::move(mutations), mutation_weights},
                   fitness_domain,
                   0.02);

    const auto& [final_population, met_criterion, state] =
        moga.run(std::move(initial_solutions),
                 [](const Population& population, const State& state) {
                     static constexpr unsigned long long log_frequency = 1000;
                     if (state.iteration % log_frequency == 0) {
                         std::size_t n_pareto = 0;
                         for (const auto& individual : population) {
                             if (individual.is_pareto_individual()) {
                                 n_pareto++;
                             }
                         }
                         std::cout << fmt::format(
                             "Iteration {:>6}: {:>3} pareto individuals.\n",
                             state.iteration,
                             n_pareto);
                     }
                 });

    std::cout << met_criterion.get_termination_message()
              << "\nNumber of iterations: " << state.iteration << '\n';

    std::cout << fmt::format("{:^18} {:^18} {:^18}\n",
                             "Individual",
                             "First objective",
                             "Second objective");
    for (const auto& individual : final_population) {
        if (individual.is_pareto_individual()) {
            std::cout << fmt::format(
                "{:>13.3e}      {:>13.3e}       {:>13.3e}\n",
                individual.solution,
                -individual.get_fitness()[0],
                -individual.get_fitness()[1]);
        }
    }
}

void exhaustivesearchso_square()
{
    using Algorithm  = SHO::SO::ExhaustiveSearch<double>;
    using Individual = Algorithm::Individual;
    using State      = Algorithm::State;

    const auto& domain = Problem::Square::domain;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(5)));

    Algorithm es(std::make_unique<Problem::Square::Evaluator>(),
                 std::move(terminations),
                 std::make_unique<SHO::FloatingPoint::PositiveAdvancement>(
                     0.0001, domain));

    const auto& initial_solution = domain.min;

    std::cout << fmt::format(
        "{:^10} {:^15} {:^15}\n", "Iteration", "Solution", "Fitness");
    const auto& [met_criterion, best_individual, state] =
        es.run(initial_solution,
               [](const Individual& /*individual*/,
                  const Individual& best_individual,
                  const State& state) {
                   static constexpr unsigned long long log_frequency = 2500000;
                   if (state.iteration % log_frequency == 0) {
                       std::cout
                           << fmt::format("{:>8}      {:> .3e}      {:> .3e}\n",
                                          state.iteration,
                                          best_individual.solution,
                                          best_individual.get_fitness());
                   }
               });

    const std::string termination_message =
        met_criterion ? met_criterion->get_termination_message() :
                        "All solutions enumerated.";
    std::cout << termination_message
              << "\nNumber of iterations: " << state.iteration
              << "\nBest solution: " << best_individual.solution
              << "\nBest fitness:  " << best_individual.get_fitness() << '\n';
}

void exhaustivesearchmo_schaffer()
{
    using Algorithm =
        SHO::MO::ExhaustiveSearch<SHO::FloatingPoint::Solution, 2>;
    using Individual = Algorithm::Individual;
    using State      = Algorithm::State;

    const auto& domain = Problem::Schaffer::domain;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(5)));

    Algorithm es(std::make_unique<Problem::Schaffer::Evaluator>(),
                 std::move(terminations),
                 std::make_unique<SHO::FloatingPoint::PositiveAdvancement>(
                     0.0001, domain));

    const auto& initial_solution = domain.min;

    const auto& [met_criterion, pareto_front, state] =
        es.run(initial_solution,
               [](const std::vector<Individual>& pareto_front,
                  const Individual& current_individual,
                  const State& state) {
                   static constexpr unsigned long long log_frequency = 500000;
                   if (state.iteration % log_frequency == 0) {
                       std::cout << fmt::format(
                           "Iteration {:>9}: {:>5} pareto individuals, current "
                           "solution: {:9.3f}\n",
                           state.iteration,
                           pareto_front.size(),
                           current_individual.solution);
                   }
               });

    const std::string termination_message =
        met_criterion ? met_criterion->get_termination_message() :
                        "All solutions enumerated.";
    std::cout << termination_message
              << "\nNumber of iterations: " << state.iteration
              << "\nPareto front size: " << pareto_front.size() << '\n';
}

void localsearch_square()
{
    using Algorithm  = SHO::SO::LocalSearch<SHO::FloatingPoint::Solution>;
    using Individual = Algorithm::Individual;
    using State      = Algorithm::State;

    const auto& domain = Problem::Square::domain;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(5)));
    terminations.push_back(
        std::make_unique<SHO::MaxIterationsWithoutImprovement>(30));

    std::vector<double> np_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::NeighborhoodProvider>> nps;
    nps.push_back(
        std::make_unique<SHO::FloatingPoint::SimpleNeighborhoodProvider>());

    Algorithm ls(std::make_unique<Problem::Square::Evaluator>(),
                 std::move(terminations),
                 {std::move(nps), std::move(np_weights)});

    const auto& initial_solution = domain.min;

    std::cout << fmt::format(
        "{:^10} {:^15} {:^15}\n", "Iteration", "Solution", "Fitness");
    const auto& [met_criterion, best_individual, state] = ls.run(
        initial_solution, [](const Individual& individual, const State& state) {
            static constexpr unsigned long long log_frequency = 30;
            if (state.iteration % log_frequency == 0) {
                std::cout << fmt::format("{:>6}    {:>13.3e}   {:>13.3e}\n",
                                         state.iteration,
                                         individual.solution,
                                         individual.get_fitness());
            }
        });

    const std::string termination_message =
        met_criterion ?
            met_criterion->get_termination_message() :
            "Reached a neighborhood containing only worse solutions.";
    std::cout << termination_message
              << "\nNumber of iterations: " << state.iteration
              << "\nBest solution: " << best_individual.solution
              << "\nBest fitness:  " << best_individual.get_fitness() << '\n';
}

void localsearch_tsp()
{
    using Algorithm  = SHO::SO::LocalSearch<SHO::PermutationVector::Solution>;
    using Individual = Algorithm::Individual;
    using State      = Algorithm::State;

    auto distance_matrix =
        Problem::TravelingSalesman::DistanceMatrix::load_from_tsplib_file(
            "../TSPLIB/bays29.tsp");

    std::vector<std::unique_ptr<SHO::TerminationCriterion>> terminations;
    terminations.push_back(
        std::make_unique<SHO::MaxRunningTime>(std::chrono::seconds(5)));
    terminations.push_back(
        std::make_unique<SHO::MaxIterationsWithoutImprovement>(30));

    std::vector<double> np_weights = {1.0};
    std::vector<std::unique_ptr<Algorithm::NeighborhoodProvider>> nps;
    nps.push_back(
        std::make_unique<SHO::PermutationVector::SwapNeighborhoodProvider>());

    Algorithm ls(std::make_unique<Problem::TravelingSalesman::Evaluator>(
                     std::move(distance_matrix)),
                 std::move(terminations),
                 {std::move(nps), std::move(np_weights)});

    const auto n_nodes = distance_matrix.get_number_of_nodes();
    SHO::PermutationVector::Solution initial_solution;
    initial_solution.reserve(n_nodes);
    for (std::size_t i = 0; i < n_nodes; i++) { initial_solution.push_back(i); }
    std::shuffle(initial_solution.begin(),
                 initial_solution.end(),
                 SHO::Random::get_generator());

    std::cout << fmt::format(" {:^10} {:^15}\n", "Iteration", "Fitness");
    const auto& [met_criterion, best_individual, state] =
        ls.run(std::move(initial_solution),
               [](const Individual& individual, const State& state) {
                   static constexpr unsigned long long log_frequency = 1;
                   if (state.iteration % log_frequency == 0) {
                       std::cout << fmt::format("{:>6}           {}\n",
                                                state.iteration,
                                                individual.get_fitness());
                   }
               });

    const std::string termination_message =
        met_criterion ?
            met_criterion->get_termination_message() :
            "Reached a neighborhood containing only worse solutions.";
    std::cout << termination_message
              << "\nNumber of iterations: " << state.iteration
              << "\nBest solution: " << best_individual.solution
              << "\nBest fitness:  " << best_individual.get_fitness() << '\n';
}

}
