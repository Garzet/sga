#ifndef SHO_TUNE_EXPERIMENTPARAMETERS_HPP
#define SHO_TUNE_EXPERIMENTPARAMETERS_HPP

#include "tune/AlgorithmParameters.hpp"
#include "tune/CrossoverParameters.hpp"
#include "tune/MutationParameters.hpp"
#include "tune/ProblemParameters.hpp"
#include "tune/SelectionParameters.hpp"
#include "tune/InitializerParameters.hpp"
#include "tune/TerminationCriterionParameters.hpp"
#include "tune/InspectorParameters.hpp"
#include "tune/WeightedEntry.hpp"

#include <string>
#include <vector>

namespace tune {

class ExperimentParameters {
  protected:
    using TerminationsParameters =
        std::vector<std::unique_ptr<TerminationCriterionParameters>>;

  private:
    TerminationsParameters terminations_parameters;

  protected:
    explicit ExperimentParameters(
        TerminationsParameters terminations_parameters) noexcept;

    const TerminationsParameters& get_terminations_parameters() const noexcept;

    std::vector<std::unique_ptr<SHO::TerminationCriterion>>
    load_termination_criteria() const;
};

template<typename Solution>
class SSGAExperimentParameters : public ExperimentParameters {
  public:
    using ProblemParameters   = tune::ProblemParameters<Solution>;
    using SelectionParameters = tune::SelectionParameters<Solution>;
    using CrossoverParameters = tune::CrossoverParameters<Solution>;
    using MutationParameters  = tune::MutationParameters<Solution>;
    using InspectorParameters = tune::SSGAInspectorParameters<Solution>;
    using InitializerParameters =
        tune::PopulationInitializerParameters<Solution>;
    using Result = typename SHO::SO::SSGA<Solution>::Result;

  private:
    std::optional<unsigned> seed;
    std::unique_ptr<ProblemParameters> problem_parameters;
    GeneticAlgorithmParameters ga_parameters;
    WeightedVector<SelectionParameters> selections_parameters;
    WeightedVector<CrossoverParameters> crossovers_parameters;
    WeightedVector<MutationParameters> mutations_parameters;
    std::unique_ptr<InitializerParameters> initializer_parameters;
    std::unique_ptr<InspectorParameters> inspector_parameters;

  public:
    SSGAExperimentParameters(
        std::unique_ptr<ProblemParameters> problem_parameters,
        GeneticAlgorithmParameters ga_parameters,
        WeightedVector<SelectionParameters> selections_parameters,
        WeightedVector<CrossoverParameters> crossovers_parameters,
        WeightedVector<MutationParameters> mutations_parameters,
        std::unique_ptr<InitializerParameters> initializer_parameters,
        TerminationsParameters terminations_parameters,
        std::unique_ptr<InspectorParameters> inspector_parameters,
        std::optional<unsigned> seed = std::nullopt);

    Result run() const;
};

template<typename Solution>
SSGAExperimentParameters<Solution>::SSGAExperimentParameters(
    std::unique_ptr<ProblemParameters> problem_parameters,
    GeneticAlgorithmParameters ga_parameters,
    WeightedVector<SelectionParameters> selections_parameters,
    WeightedVector<CrossoverParameters> crossovers_parameters,
    WeightedVector<MutationParameters> mutations_parameters,
    std::unique_ptr<InitializerParameters> initializer_parameters,
    TerminationsParameters terminations_parameters,
    std::unique_ptr<InspectorParameters> inspector_parameters,
    std::optional<unsigned> seed)
        : ExperimentParameters{std::move(terminations_parameters)},
          seed{seed},
          problem_parameters{std::move(problem_parameters)},
          ga_parameters{std::move(ga_parameters)},
          selections_parameters{std::move(selections_parameters)},
          crossovers_parameters{std::move(crossovers_parameters)},
          mutations_parameters{std::move(mutations_parameters)},
          initializer_parameters{std::move(initializer_parameters)},
          inspector_parameters{std::move(inspector_parameters)}
{
    if (this->get_terminations_parameters().empty()) {
        throw MissingMandatoryParameterError{"termination"};
    }
    if (this->selections_parameters.empty()) {
        throw MissingMandatoryParameterError{"selection"};
    }
    if (this->crossovers_parameters.empty()) {
        throw MissingMandatoryParameterError{"crossover"};
    }
    if (this->mutations_parameters.empty()) {
        throw MissingMandatoryParameterError{"mutation"};
    }
    if (!this->initializer_parameters) {
        throw MissingMandatoryParameterError{"initializer"};
    }
    if (!this->inspector_parameters) {
        throw MissingMandatoryParameterError{"inspector"};
    }
}

template<typename Solution>
auto SSGAExperimentParameters<Solution>::run() const -> Result
{
    using Algorithm  = SHO::SO::SSGA<Solution>;
    using Population = typename Algorithm::Population;
    using State      = SHO::State;

    if (seed) { SHO::Random::get_generator().seed(*seed); }

    Algorithm algorithm{problem_parameters->load(),
                        load_termination_criteria(),
                        selections_parameters.load_probability_vector(),
                        crossovers_parameters.load_probability_vector(),
                        mutations_parameters.load_probability_vector(),
                        ga_parameters.get_mutation_probability()};

    auto ssga_inspector = inspector_parameters->load();

    return algorithm.run(
        initializer_parameters->load(),
        [&ssga_inspector](const Population& population, const State& state) {
            ssga_inspector->inspect_iteration(population, state);
        });
}

}

#endif
