#include "tune/TerminationCriterionParameters.hpp"

namespace tune {

MaxRunningTimeParameters::MaxRunningTimeParameters(
    std::chrono::duration<double> duration) noexcept
        : duration{duration}
{}

std::unique_ptr<SHO::TerminationCriterion>
MaxRunningTimeParameters::load() const
{
    return std::make_unique<SHO::MaxRunningTime>(duration);
}

std::chrono::duration<double>
MaxRunningTimeParameters::get_duration() const noexcept
{
    return duration;
}

MaxEvaluationsParameters::MaxEvaluationsParameters(
    unsigned long long max_evaluations) noexcept
        : max_evaluations{max_evaluations}
{}

std::unique_ptr<SHO::TerminationCriterion>
MaxEvaluationsParameters::load() const
{
    return std::make_unique<SHO::MaxEvaluations>(max_evaluations);
}

unsigned long long
MaxEvaluationsParameters::get_max_evaluations() const noexcept
{
    return max_evaluations;
}

MaxIterationsParameters::MaxIterationsParameters(
    unsigned long long max_iterations) noexcept
        : max_iterations{max_iterations}
{}

std::unique_ptr<SHO::TerminationCriterion> MaxIterationsParameters::load() const
{
    return std::make_unique<SHO::MaxIterations>(max_iterations);
}

unsigned long long MaxIterationsParameters::get_max_iterations() const noexcept
{
    return max_iterations;
}

MaxIterationsWithoutImprovementParameters::
    MaxIterationsWithoutImprovementParameters(
        unsigned long long max_iterations_without_improvement) noexcept
        : max_iterations_without_improvement{max_iterations_without_improvement}
{}

std::unique_ptr<SHO::TerminationCriterion>
MaxIterationsWithoutImprovementParameters::load() const
{
    return std::make_unique<SHO::MaxIterationsWithoutImprovement>(
        max_iterations_without_improvement);
}

unsigned long long MaxIterationsWithoutImprovementParameters::
    get_max_iterations_without_improvement() const noexcept
{
    return max_iterations_without_improvement;
}

}
