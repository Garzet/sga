#include "tune/CrossoverParameters.hpp"

namespace tune {

namespace floating_point {

auto ArithmeticCrossoverParameters::load() const -> std::unique_ptr<Crossover>
{
    return std::make_unique<SHO::FloatingPoint::ArithmeticCrossover>();
}

}

namespace permutation_vector {

auto CycleCrossoverParameters::load() const -> std::unique_ptr<Crossover>
{
    return std::make_unique<SHO::PermutationVector::CycleCrossover>();
}

auto PartiallyMappedCrossoverParameters::load() const
    -> std::unique_ptr<Crossover>
{
    return std::make_unique<SHO::PermutationVector::PartiallyMappedCrossover>();
}

auto OrderCrossoverParameters::load() const -> std::unique_ptr<Crossover>
{
    return std::make_unique<SHO::PermutationVector::OrderCrossover>();
}

}

}
