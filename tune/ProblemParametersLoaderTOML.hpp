#ifndef SHO_TUNE_PROBLEMPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_PROBLEMPARAMETERSLOADERTOML_HPP

#include "tune/ProblemParameters.hpp"

#include <toml++/toml.h>

namespace tune {

namespace floating_point {

std::unique_ptr<ProblemParameters>
load_square_problem_parameters(const toml::table& problem);

}

namespace permutation_vector {

std::unique_ptr<ProblemParameters>
load_traveling_salesman_problem_parameters(const toml::table& problem);

}

}

#endif
