#ifndef SHO_TUNE_GRIDSEARCHPREPROCESSORTOML_HPP
#define SHO_TUNE_GRIDSEARCHPREPROCESSORTOML_HPP

#include <variant>
#include <vector>
#include <string>

#include <toml++/toml.h>

namespace tune {

class GridSearchPreprocessorTOML {
  private:
    struct GridEntry {
        toml::table* table_to_modify;
        std::string key_to_modify;
        toml::array values;
        std::size_t values_index;
    };

    struct ExperimentData {
        toml::table plain_experiment; // Experiment without grid search entries.
        std::vector<GridEntry> grid_entries;
        bool first = true;
    };

  private:
    toml::table experiment;
    std::vector<GridEntry> grid_entries;
    bool first = true;

  public:
    explicit GridSearchPreprocessorTOML(toml::table experiment);

    const toml::table* generate_next_experiment() noexcept;
    toml::array generate_all_experiments() noexcept;

  public:
    static std::vector<toml::table> preprocess(toml::array experiments);

  private:
    void extract_grid_entries(toml::table& table);
    void extract_grid_entries(toml::array& array);
};

}

#endif
