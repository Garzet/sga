#ifndef SHO_TUNE_ERROR_HPP
#define SHO_TUNE_ERROR_HPP

#include "SHO/Assert.hpp"
#include "SHO/FunctionDomain.hpp"
#include "tune/Solution.hpp"

#include <exception>
#include <filesystem>
#include <memory>
#include <optional>
#include <string>
#include <sstream>
#include <vector>
#include <variant>
#include <ostream>

namespace tune {

struct BasicTracePoint {
    std::string trace_name;
};

struct BasicIndexTracePoint {
    std::size_t index;
};

struct IndexedTracePoint {
    std::string trace_name;
    std::size_t index;
};

struct NamedTracePoint {
    std::string trace_name;
    std::string name;
};

struct NamedIndexedTracePoint {
    std::string trace_name;
    std::size_t index;
    std::string name;
};

using TracePoint = std::variant<BasicTracePoint,
                                BasicIndexTracePoint,
                                IndexedTracePoint,
                                NamedTracePoint,
                                NamedIndexedTracePoint>;

std::ostream& operator<<(std::ostream& os, const BasicTracePoint& btp);
std::ostream& operator<<(std::ostream& os, const BasicIndexTracePoint& bitp);
std::ostream& operator<<(std::ostream& os, const IndexedTracePoint& itp);
std::ostream& operator<<(std::ostream& os, const NamedTracePoint& ntp);
std::ostream& operator<<(std::ostream& os, const NamedIndexedTracePoint& nitp);

class TraceError : public std::exception {
  private:
    std::shared_ptr<std::vector<TracePoint>> trace;

  public:
    TraceError();

    void push_trace_point(TracePoint trace_point) const noexcept;

    const std::vector<TracePoint>& get_trace() const noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<TraceError>);

class MissingLoaderError final : public TraceError {
  private:
    std::shared_ptr<std::vector<std::string>> existing_keys;
    std::shared_ptr<std::string> missing_key;
    std::shared_ptr<std::string> loader_type;

  public:
    template<typename LoaderMap>
    MissingLoaderError(const LoaderMap& loaders,
                       const typename LoaderMap::key_type& missing_key,
                       std::string loader_type);

    const char* what() const noexcept final;
};
static_assert(std::is_nothrow_copy_constructible_v<MissingLoaderError>);

class InvalidNodeError : public TraceError {
  public:
    explicit InvalidNodeError(std::vector<TracePoint> initial_trace = {});

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidNodeError>);

class InvalidDomainError : public TraceError {
  public:
    const SHO::RealFunctionDomain domain;

  public:
    explicit InvalidDomainError(SHO::RealFunctionDomain domain) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidDomainError>);

class InvalidMutationProbabilityError : public TraceError {
  public:
    const double mutation_probability;

  public:
    explicit InvalidMutationProbabilityError(
        double mutation_probability) noexcept;

    const char* what() const noexcept override;
};
static_assert(
    std::is_nothrow_copy_constructible_v<InvalidMutationProbabilityError>);

class InvalidPopulationSizeError : public TraceError {
  public:
    const unsigned population_size;

  public:
    explicit InvalidPopulationSizeError(unsigned population_size) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidPopulationSizeError>);

class InvalidTournamentSizeError : public TraceError {
  public:
    const unsigned tournament_size;

  public:
    explicit InvalidTournamentSizeError(unsigned tournament_size) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidTournamentSizeError>);

class IncompatiblePopulationAndTournamentSizeError : public TraceError {
  public:
    const unsigned population_size;
    const unsigned tournament_size;

  public:
    IncompatiblePopulationAndTournamentSizeError(
        unsigned population_size, unsigned tournament_size) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<
              IncompatiblePopulationAndTournamentSizeError>);

class InvalidWeightError final : public TraceError {
  public:
    const double weight;

  public:
    explicit InvalidWeightError(double weight) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidWeightError>);

class InvalidStandardDeviationError : public TraceError {
  public:
    const double standard_deviation;

  public:
    explicit InvalidStandardDeviationError(double standard_deviation) noexcept;

    const char* what() const noexcept override;
};
static_assert(
    std::is_nothrow_copy_constructible_v<InvalidStandardDeviationError>);

class InvalidRangeError : public TraceError {
  public:
    const SHO::RealFunctionDomain range;

  public:
    explicit InvalidRangeError(SHO::RealFunctionDomain range) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidRangeError>);

class IncompatibleRangeAndDomainError : public TraceError {
  public:
    const SHO::RealFunctionDomain range;
    const SHO::RealFunctionDomain domain;

  public:
    IncompatibleRangeAndDomainError(SHO::RealFunctionDomain range,
                                    SHO::RealFunctionDomain domain) noexcept;

    const char* what() const noexcept override;
};
static_assert(
    std::is_nothrow_copy_constructible_v<IncompatibleRangeAndDomainError>);

class SolutionOutsideDomainError : public TraceError {
  public:
    const floating_point::Solution solution;
    const SHO::RealFunctionDomain domain;
    const std::optional<std::size_t> solution_index;

  public:
    SolutionOutsideDomainError(
        floating_point::Solution solution,
        SHO::RealFunctionDomain domain,
        std::optional<std::size_t> solution_index = std::nullopt) noexcept;

    const char* what() const noexcept override;
};
static_assert(std::is_nothrow_copy_constructible_v<SolutionOutsideDomainError>);

class InvalidNumberOfInitialSolutionsError : public TraceError {
  public:
    const unsigned population_size;
    const unsigned number_of_provided_solutions;

  private:
    std::shared_ptr<const std::filesystem::path> initial_solutions_file;

  public:
    InvalidNumberOfInitialSolutionsError(
        unsigned population_size,
        unsigned number_of_provided_solutions,
        std::optional<std::filesystem::path> initial_solutions_file =
            std::nullopt) noexcept;

    const char* what() const noexcept override;

    const std::filesystem::path* get_initial_solutions_file() const noexcept;
};
static_assert(
    std::is_nothrow_copy_constructible_v<InvalidNumberOfInitialSolutionsError>);

class MissingAlternativeNodeError : public TraceError {
  private:
    std::shared_ptr<std::vector<std::string>> alternatives;

  public:
    explicit MissingAlternativeNodeError(
        std::vector<std::string> alternatives) noexcept;

    const char* what() const noexcept override;

    const std::vector<std::string>& get_alternatives() const noexcept;
};
static_assert(
    std::is_nothrow_copy_constructible_v<MissingAlternativeNodeError>);

class FileOpenError : public TraceError {
  private:
    std::shared_ptr<std::filesystem::path> file;

  public:
    explicit FileOpenError(std::filesystem::path file) noexcept;

    const char* what() const noexcept override;

    const std::filesystem::path& get_file() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<FileOpenError>);

class MissingMandatoryParameterError : public TraceError {
  private:
    std::shared_ptr<std::string> parameter;

  public:
    explicit MissingMandatoryParameterError(std::string parameter) noexcept;

    const char* what() const noexcept override;

    const std::string& get_parameter() const noexcept;
};
static_assert(
    std::is_nothrow_copy_constructible_v<MissingMandatoryParameterError>);

class TsplibFileError : public TraceError {
  private:
    std::shared_ptr<std::filesystem::path> file;

  public:
    explicit TsplibFileError(std::filesystem::path file) noexcept;

    const char* what() const noexcept override;

    const std::filesystem::path& get_file() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<TsplibFileError>);

class TsplibUnsupportedEntryError : public TsplibFileError {
  private:
    std::shared_ptr<std::string> key;
    std::shared_ptr<std::string> entry;
    std::shared_ptr<std::vector<std::string>> supprted_entries;

  public:
    TsplibUnsupportedEntryError(
        std::filesystem::path file,
        std::string key,
        std::string entry,
        std::vector<std::string> supprted_entries) noexcept;

    const char* what() const noexcept override;

    const std::string& get_key() const noexcept;
    const std::string& get_entry() const noexcept;
    const std::vector<std::string>& get_supported_entries() const noexcept;
};
static_assert(
    std::is_nothrow_copy_constructible_v<TsplibUnsupportedEntryError>);

class TsplibMissingDimensionError : public TsplibFileError {
  public:
    explicit TsplibMissingDimensionError(std::filesystem::path file) noexcept;

    const char* what() const noexcept override;
};
static_assert(
    std::is_nothrow_copy_constructible_v<TsplibMissingDimensionError>);

class TsplibInvalidEdgeWeightError : public TsplibFileError {
  public:
    const std::size_t row;
    const std::size_t column;

  public:
    TsplibInvalidEdgeWeightError(std::filesystem::path file,
                                 std::size_t row,
                                 std::size_t column) noexcept;

    const char* what() const noexcept override;
};
static_assert(
    std::is_nothrow_copy_constructible_v<TsplibInvalidEdgeWeightError>);

class BasicAndGridKeyProvidedError : public TraceError {
  private:
    std::shared_ptr<std::string> key;

  public:
    explicit BasicAndGridKeyProvidedError(std::string key) noexcept;

    const char* what() const noexcept override;

    const std::string& get_key() const noexcept;
};
static_assert(
    std::is_nothrow_copy_constructible_v<BasicAndGridKeyProvidedError>);

class InvalidGridKeyError : public TraceError {
  private:
    std::shared_ptr<std::string> key;

  public:
    explicit InvalidGridKeyError(std::string key) noexcept;

    const char* what() const noexcept override;

    const std::string& get_key() const noexcept;
};
static_assert(std::is_nothrow_copy_constructible_v<InvalidGridKeyError>);

template<typename LoaderMap>
MissingLoaderError::MissingLoaderError(
    const LoaderMap& loaders,
    const typename LoaderMap::key_type& missing_key,
    std::string loader_type)
        : existing_keys{std::make_shared<std::vector<std::string>>()},
          missing_key{std::make_shared<std::string>()},
          loader_type{std::make_shared<std::string>(std::move(loader_type))}
{
    SHO_ASSERT(loaders.find(missing_key) == loaders.end(),
               "Creating missing loader error for key present in the map.");

    this->existing_keys->reserve(loaders.size());
    for (const auto& [key, loader] : loaders) {
        std::ostringstream oss;
        oss << key;
        this->existing_keys->push_back(oss.str());
    }

    std::ostringstream oss;
    oss << missing_key;
    *(this->missing_key) = oss.str();
}

}

#endif
