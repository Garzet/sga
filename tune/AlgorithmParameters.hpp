#ifndef SHO_TUNE_ALGORITHMPARAMETERS_HPP
#define SHO_TUNE_ALGORITHMPARAMETERS_HPP

namespace tune {

class GeneticAlgorithmParameters {
  private:
    double mutation_probability;
    unsigned population_size;

  public:
    GeneticAlgorithmParameters(double mutation_probability,
                               unsigned population_size);

    double get_mutation_probability() const noexcept;
    unsigned get_population_size() const noexcept;
};

}

#endif
