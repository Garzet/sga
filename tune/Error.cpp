#include "tune/Error.hpp"

#include <algorithm>
#include <cassert>

namespace tune {

std::ostream& operator<<(std::ostream& os, const BasicTracePoint& btp)
{
    return os << btp.trace_name;
}

std::ostream& operator<<(std::ostream& os, const BasicIndexTracePoint& bitp)
{
    return os << '[' << bitp.index << ']';
}

std::ostream& operator<<(std::ostream& os, const IndexedTracePoint& itp)
{
    return os << itp.trace_name << '[' << itp.index << ']';
}

std::ostream& operator<<(std::ostream& os, const NamedTracePoint& ntp)
{
    return os << ntp.trace_name << " (" << ntp.name << ')';
}

std::ostream& operator<<(std::ostream& os, const NamedIndexedTracePoint& nitp)
{
    return os << nitp.trace_name << '[' << nitp.index << ']' << " ("
              << nitp.name << ')';
}

TraceError::TraceError() : trace{std::make_shared<std::vector<TracePoint>>()}
{
    static constexpr std::size_t reserve_size = 8;
    this->trace->reserve(reserve_size);
}

void TraceError::push_trace_point(TracePoint trace_point) const noexcept
{
    trace->push_back(std::move(trace_point));
}

auto TraceError::get_trace() const noexcept -> const std::vector<TracePoint>&
{
    return *trace;
}

const char* TraceError::what() const noexcept
{
    return "trace error";
}

const char* MissingLoaderError::what() const noexcept
{
    return "missing loader error";
}

InvalidNodeError::InvalidNodeError(std::vector<TracePoint> initial_trace)
{
    for (auto& trace_point : initial_trace) {
        push_trace_point(std::move(trace_point));
    }
}

const char* InvalidNodeError::what() const noexcept
{
    return "invalid/missing node error";
}

InvalidDomainError::InvalidDomainError(SHO::RealFunctionDomain domain) noexcept
        : domain{domain}
{
    SHO_ASSERT_FMT(!domain.is_valid(),
                   "Creating invalid domain error for valid interval {}.",
                   domain);
}

const char* InvalidDomainError::what() const noexcept
{
    return "invalid domain error";
}

InvalidMutationProbabilityError::InvalidMutationProbabilityError(
    double mutation_probability) noexcept
        : mutation_probability{mutation_probability}
{
    SHO_ASSERT_FMT(mutation_probability < 0.0 || mutation_probability > 1.0,
                   "Creating invalid mutation probability error with a valid "
                   "probability ({}).",
                   mutation_probability);
}

const char* InvalidMutationProbabilityError::what() const noexcept
{
    return "invalid mutation probability error";
}

InvalidPopulationSizeError::InvalidPopulationSizeError(
    unsigned population_size) noexcept
        : population_size{population_size}
{
    SHO_ASSERT_FMT(population_size < 1,
                   "Creating invalid population size error with a valid "
                   "population size ({}).",
                   population_size);
}

const char* InvalidPopulationSizeError::what() const noexcept
{
    return "invalid population size error";
}

InvalidTournamentSizeError::InvalidTournamentSizeError(
    unsigned tournament_size) noexcept
        : tournament_size{tournament_size}
{
    SHO_ASSERT_FMT(tournament_size < 3,
                   "Creating invalid tournament size error with a valid "
                   "tournament size ({}).",
                   tournament_size);
}

const char* InvalidTournamentSizeError::what() const noexcept
{
    return "invalid tournament size error";
}

IncompatiblePopulationAndTournamentSizeError::
    IncompatiblePopulationAndTournamentSizeError(
        unsigned population_size, unsigned tournament_size) noexcept
        : population_size{population_size}, tournament_size{tournament_size}
{
    SHO_ASSERT_FMT(
        population_size < tournament_size,
        "Creating incompatible population and tournament size error with "
        "compatible population size ({}) and tournament size ({}).",
        population_size,
        tournament_size);
}

const char* IncompatiblePopulationAndTournamentSizeError::what() const noexcept
{
    return "incompatible population and tournament size error";
}

InvalidWeightError::InvalidWeightError(double weight) noexcept : weight{weight}
{
    SHO_ASSERT_FMT(weight <= 0.0,
                   "Creating invalid weight error with a valid weight ({}).",
                   weight);
}

const char* InvalidWeightError::what() const noexcept
{
    return "invalid weight error";
}

InvalidStandardDeviationError::InvalidStandardDeviationError(
    double standard_deviation) noexcept
        : standard_deviation{standard_deviation}
{
    SHO_ASSERT_FMT(standard_deviation < 0.0,
                   "Creating invalid standard deviation error with a valid "
                   "standard deviation ({}).",
                   standard_deviation);
}

const char* InvalidStandardDeviationError::what() const noexcept
{
    return "invalid standard deviation error";
}

InvalidRangeError::InvalidRangeError(SHO::RealFunctionDomain range) noexcept
        : range{range}
{
    SHO_ASSERT_FMT(!range.is_valid(),
                   "Creating invalid range error for valid interval {}.",
                   range);
}

const char* InvalidRangeError::what() const noexcept
{
    return "invalid range error";
}

IncompatibleRangeAndDomainError::IncompatibleRangeAndDomainError(
    SHO::RealFunctionDomain range, SHO::RealFunctionDomain domain) noexcept
        : range{range}, domain{domain}
{
    SHO_ASSERT_FMT(!domain.contains(range),
                   "Creating incompatible range and domain error with "
                   "compatible range ({}) and domain ({}).",
                   range,
                   domain);
}

const char* IncompatibleRangeAndDomainError::what() const noexcept
{
    return "incompatible range and domain error";
}

SolutionOutsideDomainError::SolutionOutsideDomainError(
    floating_point::Solution solution,
    SHO::RealFunctionDomain domain,
    std::optional<std::size_t> solution_index) noexcept
        : solution{solution}, domain{domain}, solution_index{solution_index}
{
    SHO_ASSERT_FMT(
        domain.is_valid(),
        "Creating solution outside domain error with invalid domain ({}).",
        domain);
    SHO_ASSERT_FMT(!domain.contains(solution),
                   "Creating solution outside domain error with a solution "
                   "({}) inside the domain ({}).",
                   solution,
                   domain);
}

const char* SolutionOutsideDomainError::what() const noexcept
{
    return "solution outside domain error";
}

InvalidNumberOfInitialSolutionsError::InvalidNumberOfInitialSolutionsError(
    unsigned population_size,
    unsigned number_of_provided_solutions,
    std::optional<std::filesystem::path> initial_solutions_file) noexcept
        : population_size{population_size},
          number_of_provided_solutions{number_of_provided_solutions},
          initial_solutions_file{nullptr}
{
    SHO_ASSERT(population_size > 0, "Population size must be zero.");

    if (initial_solutions_file) {
        this->initial_solutions_file = std::make_shared<std::filesystem::path>(
            std::move(*initial_solutions_file));
    }
}

const char* InvalidNumberOfInitialSolutionsError::what() const noexcept
{
    return "invalid number of initial solutions error";
}

const std::filesystem::path*
InvalidNumberOfInitialSolutionsError::get_initial_solutions_file()
    const noexcept
{
    return initial_solutions_file.get();
}

MissingAlternativeNodeError::MissingAlternativeNodeError(
    std::vector<std::string> alternatives) noexcept
        : alternatives{std::make_shared<std::vector<std::string>>(
              std::move(alternatives))}
{
    SHO_ASSERT_FMT(
        this->alternatives->size() >= 2,
        "Creating missing alternatives error for vector with {} alternatives.",
        this->alternatives->size());
}

const char* MissingAlternativeNodeError::what() const noexcept
{
    return "missing alternative node error";
}

const std::vector<std::string>&
MissingAlternativeNodeError::get_alternatives() const noexcept
{
    return *alternatives;
}

FileOpenError::FileOpenError(std::filesystem::path file) noexcept
        : file{std::make_shared<std::filesystem::path>(std::move(file))}
{}

const char* FileOpenError::what() const noexcept
{
    return "file open error";
}

const std::filesystem::path& FileOpenError::get_file() const noexcept
{
    return *file;
}

MissingMandatoryParameterError::MissingMandatoryParameterError(
    std::string parameter) noexcept
        : parameter{std::make_shared<std::string>(std::move(parameter))}
{}

const char* MissingMandatoryParameterError::what() const noexcept
{
    return "missing mandatory parameter error";
}

const std::string&
MissingMandatoryParameterError::get_parameter() const noexcept
{
    return *parameter;
}

TsplibFileError::TsplibFileError(std::filesystem::path file) noexcept
        : file{std::make_shared<std::filesystem::path>(std::move(file))}
{}

const char* TsplibFileError::what() const noexcept
{
    return "TSPLIB file error";
}

const std::filesystem::path& TsplibFileError::get_file() const noexcept
{
    return *file;
}

TsplibUnsupportedEntryError::TsplibUnsupportedEntryError(
    std::filesystem::path file,
    std::string key,
    std::string entry,
    std::vector<std::string> supprted_entries) noexcept
        : TsplibFileError{std::move(file)},
          key{std::make_shared<std::string>(std::move(key))},
          entry{std::make_shared<std::string>(std::move(entry))},
          supprted_entries{std::make_shared<std::vector<std::string>>(
              std::move(supprted_entries))}
{
    SHO_ASSERT_FMT(std::find(this->supprted_entries->begin(),
                             this->supprted_entries->end(),
                             *this->entry) == this->supprted_entries->end(),
                   "Creating unsupported TSP file entry error for key {} and "
                   "supported entry {}.",
                   *this->key,
                   *this->entry);
}

const char* TsplibUnsupportedEntryError::what() const noexcept
{
    return "TSPLIB unsupported entry error";
}

const std::string& TsplibUnsupportedEntryError::get_key() const noexcept
{
    return *key;
}

const std::string& TsplibUnsupportedEntryError::get_entry() const noexcept
{
    return *entry;
}

const std::vector<std::string>&
TsplibUnsupportedEntryError::get_supported_entries() const noexcept
{
    return *supprted_entries;
}

TsplibMissingDimensionError::TsplibMissingDimensionError(
    std::filesystem::path file) noexcept
        : TsplibFileError{std::move(file)}
{}

const char* TsplibMissingDimensionError::what() const noexcept
{
    return "TSPLIB missing dimension error";
}

TsplibInvalidEdgeWeightError::TsplibInvalidEdgeWeightError(
    std::filesystem::path file, std::size_t row, std::size_t column) noexcept
        : TsplibFileError{std::move(file)}, row{row}, column{column}
{}

const char* TsplibInvalidEdgeWeightError::what() const noexcept
{
    return "TSPLIB invalid edge weight error";
}

BasicAndGridKeyProvidedError::BasicAndGridKeyProvidedError(
    std::string key) noexcept
        : key{std::make_shared<std::string>(std::move(key))}
{}

const char* BasicAndGridKeyProvidedError::what() const noexcept
{
    return "Basic and grid key provided error";
}

const std::string& BasicAndGridKeyProvidedError::get_key() const noexcept
{
    return *key;
}

InvalidGridKeyError::InvalidGridKeyError(std::string key) noexcept
        : key{std::make_shared<std::string>(std::move(key))}
{}

const char* InvalidGridKeyError::what() const noexcept
{
    return "Invalid grid key error";
}

const std::string& InvalidGridKeyError::get_key() const noexcept
{
    return *key;
}
}
