#ifndef SHO_TUNE_SOLUTION_HPP
#define SHO_TUNE_SOLUTION_HPP

// TODO: At one point, the core SHO solutions and tune solutions should be
//       synchronized (namespaces names and solution names). This should remove
//       the need for this header.

#include "SHO/Solution.hpp"

namespace tune {

namespace binary_vector {

using Solution = SHO::BinaryVector::Solution;

}

namespace floating_point {

using Solution = SHO::FloatingPoint::Solution;

}

namespace permutation_vector {

using Solution = SHO::PermutationVector::Solution;

}

}

#endif
