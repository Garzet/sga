#ifndef SHO_TUNE_INSPECTORPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_INSPECTORPARAMETERSLOADERTOML_HPP

#include "tune/Error.hpp"
#include "tune/InspectorParameters.hpp"

#include <toml++/toml.h>

#include <iostream>
#include <optional>

namespace tune {

template<typename Solution>
class SSGAInspectorParametersLoaderTOML {
  public:
    using SSGAInspectorParameters = tune::SSGAInspectorParameters<Solution>;

  public:
    virtual ~SSGAInspectorParametersLoaderTOML() = default;

    virtual std::unique_ptr<SSGAInspectorParameters>
    load(const toml::table& inspector) const = 0;
};

template<typename Solution>
class NullSSGAInspectorParametersLoaderTOML final
        : public SSGAInspectorParametersLoaderTOML<Solution> {
  public:
    using SSGAInspectorParameters = tune::SSGAInspectorParameters<Solution>;
    using NullSSGAInspectorParameters =
        tune::NullSSGAInspectorParameters<Solution>;

  public:
    std::unique_ptr<SSGAInspectorParameters>
    load(const toml::table& inspector) const final;
};

template<typename Solution>
class LogSSGAInspectorParametersLoaderTOML
        : public SSGAInspectorParametersLoaderTOML<Solution> {
  public:
    using SSGAInspectorParameters = tune::SSGAInspectorParameters<Solution>;
    using LogSSGAInspectorParameters =
        tune::LogSSGAInspectorParameters<Solution>;

  protected:
    std::ostream& os;

  public:
    explicit LogSSGAInspectorParametersLoaderTOML(
        std::ostream& os = std::cout) noexcept;

    std::unique_ptr<SSGAInspectorParameters>
    load(const toml::table& inspector) const override;

  protected:
    std::chrono::duration<double>
    load_frequency(const toml::table& inspector) const;
};

template<typename Solution>
LogSSGAInspectorParametersLoaderTOML<
    Solution>::LogSSGAInspectorParametersLoaderTOML(std::ostream& os) noexcept
        : os{os}
{}

template<typename Solution>
auto NullSSGAInspectorParametersLoaderTOML<Solution>::load(
    const toml::table& /*inspector*/) const
    -> std::unique_ptr<SSGAInspectorParameters>
{
    return std::make_unique<NullSSGAInspectorParameters>();
}

template<typename Solution>
auto LogSSGAInspectorParametersLoaderTOML<Solution>::load(
    const toml::table& inspector) const
    -> std::unique_ptr<SSGAInspectorParameters>
{
    return std::make_unique<LogSSGAInspectorParameters>(
        os, load_frequency(inspector));
}

template<typename Solution>
auto LogSSGAInspectorParametersLoaderTOML<Solution>::load_frequency(
    const toml::table& inspector) const -> std::chrono::duration<double>
{
    const auto time = inspector["frequency"].value<toml::time>();
    if (!time) { throw InvalidNodeError{{BasicTracePoint{"frequency"}}}; }
    return std::chrono::hours{time->hour} + std::chrono::minutes{time->minute} +
           std::chrono::seconds{time->second} +
           std::chrono::nanoseconds{time->nanosecond};
}

}

#endif
