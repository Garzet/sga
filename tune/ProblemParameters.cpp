#include "tune/ProblemParameters.hpp"

#include "tune/Error.hpp"

namespace tune {

namespace floating_point {

ProblemParameters::ProblemParameters(SHO::RealFunctionDomain domain)
        : domain{domain}
{
    if (!domain.is_valid()) { throw InvalidDomainError{domain}; }
}

SHO::RealFunctionDomain ProblemParameters::get_domain() const noexcept
{
    return domain;
}

SquareProblemParameters::SquareProblemParameters(SHO::RealFunctionDomain domain)
        : ProblemParameters{domain}
{}

auto SquareProblemParameters::load() const -> std::unique_ptr<Evaluator>
{
    return std::make_unique<SquareProblem::Evaluator>(get_domain());
}

}

namespace permutation_vector {

ProblemParameters::ProblemParameters(std::size_t solution_size) noexcept
        : solution_size{solution_size}
{}

std::size_t ProblemParameters::get_solution_size() const noexcept
{
    return solution_size;
}

TravelingSalesmanProblemParameters::TravelingSalesmanProblemParameters(
    TravelingSalesmanProblem::DistanceMatrix distance_matrix)
        : ProblemParameters{distance_matrix.get_number_of_nodes()},
          distance_matrix{std::move(distance_matrix)}
{}

auto TravelingSalesmanProblemParameters::load() const
    -> std::unique_ptr<Evaluator>
{
    return std::make_unique<TravelingSalesmanProblem::Evaluator>(
        distance_matrix);
}

}

}
