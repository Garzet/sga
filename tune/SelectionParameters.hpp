#ifndef SHO_TUNE_SELECTIONPARAMETERS_HPP
#define SHO_TUNE_SELECTIONPARAMETERS_HPP

#include "SHO/SO/SSGA.hpp"
#include "SHO/Assert.hpp"

#include "tune/Error.hpp"

#include <memory>
#include <sstream>

namespace tune {

template<typename Solution>
class SelectionParameters {
  public:
    using Selection = typename SHO::SO::SSGA<Solution>::Selection;

  public:
    virtual ~SelectionParameters() = default;

    virtual std::unique_ptr<Selection> load() const = 0;
};

template<typename Solution>
class TournamentSelectionParameters : public SelectionParameters<Solution> {
  public:
    using Selection = typename SelectionParameters<Solution>::Selection;

  private:
    unsigned tournament_size;
    unsigned population_size;

  public:
    TournamentSelectionParameters(unsigned tournament_size,
                                  unsigned population_size);

    std::unique_ptr<Selection> load() const final;

    unsigned get_tournament_size() const noexcept;
};

template<typename Solution>
class RouletteWheelSelectionParameters : public SelectionParameters<Solution> {
  public:
    using Selection = typename SelectionParameters<Solution>::Selection;

  private:
    unsigned population_size;

  public:
    explicit RouletteWheelSelectionParameters(unsigned population_size);

    std::unique_ptr<Selection> load() const final;
};

template<typename Solution>
TournamentSelectionParameters<Solution>::TournamentSelectionParameters(
    unsigned tournament_size, unsigned population_size)
        : tournament_size{tournament_size}, population_size{population_size}
{
    // Population size is validated by the algorithm parameters object.
    SHO_ASSERT(population_size > 0, "Population size must be at least one.");

    if (tournament_size > population_size) {
        throw IncompatiblePopulationAndTournamentSizeError{population_size,
                                                           tournament_size};
    }
    if (tournament_size < 3) {
        throw InvalidTournamentSizeError{tournament_size};
    }
}

template<typename Solution>
auto TournamentSelectionParameters<Solution>::load() const
    -> std::unique_ptr<Selection>
{
    using TournamentSelection =
        typename SHO::SO::SSGA<Solution>::TournamentSelection;
    return std::make_unique<TournamentSelection>(population_size,
                                                 tournament_size);
}

template<typename Solution>
unsigned
TournamentSelectionParameters<Solution>::get_tournament_size() const noexcept
{
    return tournament_size;
}

template<typename Solution>
RouletteWheelSelectionParameters<Solution>::RouletteWheelSelectionParameters(
    unsigned population_size)
        : population_size{population_size}
{
    // Valid population size is always expected (validated by the algorithm).
    SHO_ASSERT(population_size > 0, "Population size must be at least one.");
}

template<typename Solution>
auto RouletteWheelSelectionParameters<Solution>::load() const
    -> std::unique_ptr<Selection>
{
    using RouletteWheel = typename SHO::SO::SSGA<Solution>::RouletteWheel;
    return std::make_unique<RouletteWheel>(population_size);
}

}

#endif
