#include "tune/MutationParameters.hpp"

#include "SHO/Assert.hpp"
#include "tune/Error.hpp"

#include <sstream>

namespace tune {

namespace floating_point {

GaussianMutationParameters::GaussianMutationParameters(
    double standard_deviation)
        : standard_deviation(standard_deviation)
{
    if (standard_deviation < 0.0) {
        throw InvalidStandardDeviationError{standard_deviation};
    }
}

auto GaussianMutationParameters::load() const -> std::unique_ptr<Mutation>
{
    return std::make_unique<SHO::FloatingPoint::GaussianMutation>(
        standard_deviation);
}

double GaussianMutationParameters::get_standard_deviation() const noexcept
{
    return standard_deviation;
}

RandomMutationParameters::RandomMutationParameters(
    SHO::RealFunctionDomain range, SHO::RealFunctionDomain domain)
        : range{range}
{
    // Domain is validated by the problem parameters object.
    SHO_ASSERT(domain.is_valid(), "Invaild domain.");

    if (!range.is_valid()) { throw InvalidRangeError{range}; }
    if (!domain.contains(range)) {
        throw IncompatibleRangeAndDomainError{range, domain};
    }
}

auto RandomMutationParameters::load() const -> std::unique_ptr<Mutation>
{
    return std::make_unique<SHO::FloatingPoint::RandomMutation>(range.min,
                                                                range.max);
}

SHO::RealFunctionDomain RandomMutationParameters::get_range() const noexcept
{
    return range;
}

}

namespace permutation_vector {

auto SwapMutationParameters::load() const -> std::unique_ptr<Mutation>
{
    return std::make_unique<SHO::PermutationVector::SwapMutation>();
}

auto RandomMutationParameters::load() const -> std::unique_ptr<Mutation>
{
    return std::make_unique<SHO::PermutationVector::RandomMutation>();
}

}

}
