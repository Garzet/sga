#ifndef SHO_TUNE_CROSSOVERPARAMETERS_HPP
#define SHO_TUNE_CROSSOVERPARAMETERS_HPP

#include "tune/Solution.hpp"

#include "SHO/Crossover.hpp"

#include <memory>

namespace tune {

template<typename Solution>
class CrossoverParameters {
  public:
    using Crossover = SHO::Crossover<Solution>;

  public:
    virtual ~CrossoverParameters() = default;

    virtual std::unique_ptr<Crossover> load() const = 0;
};

namespace floating_point {

class ArithmeticCrossoverParameters final
        : public CrossoverParameters<Solution> {
  public:
    std::unique_ptr<Crossover> load() const final;
};

}

namespace permutation_vector {

class CycleCrossoverParameters final : public CrossoverParameters<Solution> {
  public:
    std::unique_ptr<Crossover> load() const final;
};

class PartiallyMappedCrossoverParameters final
        : public CrossoverParameters<Solution> {
  public:
    std::unique_ptr<Crossover> load() const final;
};

class OrderCrossoverParameters final : public CrossoverParameters<Solution> {
  public:
    std::unique_ptr<Crossover> load() const final;
};

}

}

#endif
