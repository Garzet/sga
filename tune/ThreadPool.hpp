#ifndef SHO_TUNE_THREADPOOL_HPP
#define SHO_TUNE_THREADPOOL_HPP

#include <vector>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <functional>

namespace tune {

class ThreadPool {
  private:
    std::vector<std::thread> threads;

    std::mutex tasks_mutex;
    std::condition_variable tasks_condition_variable;
    std::queue<std::function<void()>> tasks;

    std::atomic<bool> terminate_pool;

  public:
    explicit ThreadPool(unsigned n_threads);
    ~ThreadPool() noexcept;

    ThreadPool(const ThreadPool& other) = delete;
    ThreadPool(ThreadPool&& other)      = delete;
    ThreadPool& operator=(const ThreadPool& other) = delete;
    ThreadPool& operator=(const ThreadPool&& other) = delete;

    void add_task(std::function<void()> task);

    unsigned get_n_threads() const noexcept;

  private:
    void perform_worker_loop();
};

}

#endif
