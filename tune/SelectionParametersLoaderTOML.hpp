#ifndef SHO_TUNE_SELECTIONPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_SELECTIONPARAMETERSLOADERTOML_HPP

#include "tune/AlgorithmParameters.hpp"
#include "tune/ProblemParameters.hpp"
#include "tune/SelectionParameters.hpp"

#include <toml++/toml.h>

namespace tune {

template<typename Solution>
std::unique_ptr<SelectionParameters<Solution>>
load_tournament_selection_parameters(
    const GeneticAlgorithmParameters& ga_parameters,
    const toml::table& selection)
{
    const auto tournament_size = selection["tournament_size"].value<unsigned>();
    if (!tournament_size) {
        throw InvalidNodeError{{BasicTracePoint{"tournament_size"}}};
    }

    return std::make_unique<TournamentSelectionParameters<Solution>>(
        *tournament_size, ga_parameters.get_population_size());
}

template<typename Solution>
std::unique_ptr<SelectionParameters<Solution>>
load_roulette_wheel_selection_parameters(
    const GeneticAlgorithmParameters& ga_parameters,
    const toml::table& /*selection*/)
{
    return std::make_unique<RouletteWheelSelectionParameters<Solution>>(
        ga_parameters.get_population_size());
}

}

#endif
