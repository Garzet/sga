#ifndef SHO_TUNE_MUTATIONPARAMETERS_HPP
#define SHO_TUNE_MUTATIONPARAMETERS_HPP

#include "SHO/Mutation.hpp"

#include "tune/Solution.hpp"

#include <memory>

namespace tune {

template<typename Solution>
class MutationParameters {
  public:
    using Mutation = SHO::Mutation<Solution>;

  public:
    virtual ~MutationParameters() = default;

    virtual std::unique_ptr<Mutation> load() const = 0;
};

namespace floating_point {

using MutationParameters = tune::MutationParameters<Solution>;

class GaussianMutationParameters final : public MutationParameters {
  private:
    double standard_deviation;

  public:
    explicit GaussianMutationParameters(double standard_deviation);

    std::unique_ptr<Mutation> load() const final;

    double get_standard_deviation() const noexcept;
};

class RandomMutationParameters final : public MutationParameters {
  private:
    SHO::RealFunctionDomain range;

  public:
    RandomMutationParameters(SHO::RealFunctionDomain range,
                             SHO::RealFunctionDomain domain);

    std::unique_ptr<Mutation> load() const final;

    SHO::RealFunctionDomain get_range() const noexcept;
};

}

namespace permutation_vector {

using MutationParameters = tune::MutationParameters<Solution>;

class SwapMutationParameters final : public MutationParameters {
  public:
    std::unique_ptr<Mutation> load() const final;
};

class RandomMutationParameters final : public MutationParameters {
  public:
    std::unique_ptr<Mutation> load() const final;
};

}

}

#endif
