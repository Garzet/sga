#include "tune/AlgorithmParameters.hpp"

#include "tune/Error.hpp"

namespace tune {

GeneticAlgorithmParameters::GeneticAlgorithmParameters(
    double mutation_probability, unsigned population_size)
        : mutation_probability{mutation_probability},
          population_size{population_size}
{
    if (mutation_probability < 0.0 || mutation_probability > 1.0) {
        throw InvalidMutationProbabilityError{mutation_probability};
    }
    if (population_size < 1) {
        throw InvalidPopulationSizeError{population_size};
    }
}

double GeneticAlgorithmParameters::get_mutation_probability() const noexcept
{
    return mutation_probability;
}

unsigned GeneticAlgorithmParameters::get_population_size() const noexcept
{
    return population_size;
}

}
