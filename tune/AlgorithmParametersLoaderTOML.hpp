#ifndef SHO_TUNE_ALGORITHMPARAMETERSLOADERTOML_HPP
#define SHO_TUNE_ALGORITHMPARAMETERSLOADERTOML_HPP

#include "tune/AlgorithmParameters.hpp"

#include <toml++/toml.h>

namespace tune {

GeneticAlgorithmParameters
load_genetic_algorithm_parameters(const toml::table& algorithm);

}

#endif
