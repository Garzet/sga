#ifndef SHO_ADVANEMENT_HPP
#define SHO_ADVANEMENT_HPP

namespace SHO {

template<typename Solution>
class Advancement {
  public:
    virtual ~Advancement() noexcept                           = default;
    [[nodiscard]] virtual bool operator()(Solution& solution) = 0;

  protected:
    Advancement() noexcept                                    = default;
    Advancement(const Advancement& other) noexcept            = default;
    Advancement& operator=(const Advancement& other) noexcept = default;
    Advancement(Advancement&& other) noexcept                 = default;
    Advancement& operator=(Advancement&& other) noexcept      = default;
};

}

#endif
