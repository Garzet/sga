#ifndef SHO_SO_INDIVIDUAL_HPP
#define SHO_SO_INDIVIDUAL_HPP

#include "SHO/Assert.hpp"

#include <limits>

namespace SHO::SO {

template<typename Solution>
class Individual {
  private:
    constexpr static auto unevaluated =
        std::numeric_limits<double>::quiet_NaN();

  public:
    Solution solution;

  private:
    double fitness;

  public:
    explicit Individual(Solution solution) noexcept;
    Individual(Solution solution, double fitness) noexcept;

    void set_fitness(double fitness) noexcept;
    double get_fitness() const noexcept;
    bool is_evaluated() const noexcept;
};

template<typename Solution>
Individual<Solution>::Individual(Solution solution) noexcept
        : solution(std::move(solution)), fitness(unevaluated)
{}

template<typename Solution>
Individual<Solution>::Individual(Solution solution, double fitness) noexcept
        : solution(std::move(solution)), fitness(fitness)
{
    SHO_ASSERT_FMT(fitness != unevaluated,
                   "Solution fitness cannot be set to the unevaluated ({}) "
                   "constant. Use different constructor.",
                   unevaluated);
}

template<typename Solution>
bool Individual<Solution>::is_evaluated() const noexcept
{
    return !std::isnan(fitness);
}

template<typename Solution>
void Individual<Solution>::set_fitness(double fitness) noexcept
{
    SHO_ASSERT_FMT(
        fitness != unevaluated,
        "Solution fitness cannot be set to the unevaluated ({}) constant.",
        unevaluated);
    this->fitness = fitness;
}

template<typename Solution>
double Individual<Solution>::get_fitness() const noexcept
{
    SHO_ASSERT(is_evaluated(), "Solution not yet evaluated.");
    return fitness;
}

}

#endif
