#ifndef SHO_SO_EVALUATOR_HPP
#define SHO_SO_EVALUATOR_HPP

namespace SHO::SO {

template<typename Solution>
class Evaluator {
  public:
    virtual ~Evaluator() noexcept = default;

    [[nodiscard]] virtual double operator()(const Solution& solution) = 0;

  protected:
    Evaluator() noexcept                                  = default;
    Evaluator(const Evaluator& other) noexcept            = default;
    Evaluator& operator=(const Evaluator& other) noexcept = default;
    Evaluator(Evaluator& other) noexcept                  = default;
    Evaluator& operator=(Evaluator& other) noexcept       = default;
};

}

#endif
