#ifndef SHO_SO_EXHAUSTIVESEARCH_HPP
#define SHO_SO_EXHAUSTIVESEARCH_HPP

#include "SHO/State.hpp"
#include "SHO/TerminationCriterion.hpp"
#include "SHO/Advancement.hpp"

#include "SHO/SO/Evaluator.hpp"
#include "SHO/SO/Individual.hpp"

#include <chrono>
#include <functional>
#include <utility>
#include <vector>
#include <type_traits>

namespace SHO::SO {

template<typename Solution>
class ExhaustiveSearch final {
  public:
    using State       = SHO::State;
    using Advancement = SHO::Advancement<Solution>;
    using Evaluator   = SHO::SO::Evaluator<Solution>;
    using Individual  = SHO::SO::Individual<Solution>;

    struct Result final {
        const TerminationCriterion* met_criterion;
        Individual best_individual;
        State state;
    };

    using Inspector = std::function<void(
        const Individual& current_individual, const Individual& best_individual, const State& state)>;

  public:
    ExhaustiveSearch() noexcept                                         = delete;
    ~ExhaustiveSearch() noexcept                                        = delete;
    ExhaustiveSearch(const ExhaustiveSearch& other) noexcept            = delete;
    ExhaustiveSearch& operator=(const ExhaustiveSearch& other) noexcept = delete;
    ExhaustiveSearch(ExhaustiveSearch& other) noexcept                  = delete;
    ExhaustiveSearch& operator=(ExhaustiveSearch& other) noexcept       = delete;

    [[nodiscard]] static Result
    run(Evaluator& evaluator,
        const std::vector<std::reference_wrapper<TerminationCriterion>>& termination_criteria,
        Advancement& advancement,
        Solution initial_solution,
        Inspector inspector = nullptr);

  private:
    static void update_algorithm_state(double& best_fitness,
                                       State& state,
                                       std::chrono::high_resolution_clock::time_point start_timepoint,
                                       const Individual& individual) noexcept;
};

template<typename Solution>
auto ExhaustiveSearch<Solution>::run(
    Evaluator& evaluator,
    const std::vector<std::reference_wrapper<TerminationCriterion>>& termination_criteria,
    Advancement& advancement,
    Solution initial_solution,
    Inspector inspector) -> Result
{
    Individual individual{std::move(initial_solution)};
    individual.set_fitness(evaluator(individual.solution));

    Individual best_individual{individual};
    State state{0, 0, 1, std::chrono::seconds{0}};
    double best_fitness{individual.get_fitness()};

    if (inspector) { inspector(individual, best_individual, state); }

    const auto start_timepoint = std::chrono::high_resolution_clock::now();
    const TerminationCriterion* met_criterion{nullptr};
    while (!(met_criterion = check_termination_critera(termination_criteria, state)) &&
           advancement(individual.solution)) {
        individual.set_fitness(evaluator(individual.solution));

        if (individual.get_fitness() > best_individual.get_fitness()) { best_individual = individual; }

        update_algorithm_state(best_fitness, state, start_timepoint, individual);
        if (inspector) { inspector(individual, best_individual, state); }
    }

    return Result{met_criterion, std::move(best_individual), std::move(state)};
}

template<typename Solution>
void ExhaustiveSearch<Solution>::update_algorithm_state(double& best_fitness,
                                                        State& state,
                                                        std::chrono::high_resolution_clock::time_point start_timepoint,
                                                        const Individual& individual) noexcept
{
    ++state.iteration;
    if (best_fitness >= individual.get_fitness()) {
        ++state.n_iterations_without_improvement;
    } else {
        state.n_iterations_without_improvement = 0;
        best_fitness                           = individual.get_fitness();
    }
    ++state.n_evaluations; // Single evaluation per iteration.
    state.running_time = std::chrono::high_resolution_clock::now() - start_timepoint;
}

}

#endif
