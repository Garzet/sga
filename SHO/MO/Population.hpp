#ifndef SHO_MO_POPULATION_HPP
#define SHO_MO_POPULATION_HPP

#include "SHO/Assert.hpp"
#include "SHO/MO/Individual.hpp"

#include <vector>
#include <cmath>
#include <optional>
#include <algorithm>

namespace SHO::MO {

template<typename Solution, std::size_t N>
class Population : public std::vector<Individual<Solution, N>> {
    static_assert(N > 1, "Number of objectives must be greater than one.");

  public:
    using FitnessDomain = SHO::MO::FitnessDomain<N>;
    using Individual    = SHO::MO::Individual<Solution, N>;
    using ConstIterator = typename std::vector<Individual>::const_iterator;

  public:
    class Front {
      public:
        class Element {
          private:
            Individual* individual;
            std::optional<double> crowding_distance;

          public:
            explicit Element(Individual& individual) noexcept;

            Individual& get_individual() const noexcept;

            void set_crowding_distance(double crowding_distance) noexcept;
            double get_crowding_distance() const noexcept;
        };

        using ConstIterator = typename std::vector<Element>::const_iterator;

      private:
        std::vector<Element> elements;

      public:
        Front() noexcept;

        void add(Individual& individual) noexcept;
        bool contains(const Individual& individual) const noexcept;

        void crowding_sort(const FitnessDomain& domain) noexcept;

        void reserve(std::size_t reserve_size) noexcept;

        bool empty() const noexcept;
        std::size_t size() const noexcept;

        const Element& operator[](std::size_t) const noexcept;

        ConstIterator begin() const noexcept;
        ConstIterator end() const noexcept;

      private:
        void calculate_crowding_distances(const FitnessDomain& domain) noexcept;
    };

    class Fronts : private std::vector<Front> {
      public:
        Fronts() noexcept;

        void add(Front front) noexcept;

        using std::vector<Front>::operator[];

        using std::vector<Front>::empty;
        using std::vector<Front>::size;

        using std::vector<Front>::begin;
        using std::vector<Front>::end;

      private:
        bool contains_any_from_front(const Front& front) const noexcept;
    };

  private:
    class SortingData {
      public:
        class Element {
          private:
            const Individual* individual;
            std::vector<Individual*> dominated_individuals;

          public:
            unsigned dominated_by_counter;
            unsigned rank;

          public:
            explicit Element(const Individual& individual) noexcept;

            void add_dominated_individual(Individual& i) noexcept;

            const Individual& get_individual() const noexcept;

            const std::vector<Individual*>&
            get_dominated_individuals() const noexcept;
        };

      private:
        std::vector<Element> elements;

      public:
        explicit SortingData(const Population& population) noexcept;

        void add(Element element) noexcept;
        Element& get_element_for_individual(const Individual& i) noexcept;

        const std::vector<Element>& get_elements() const noexcept;
    };

  public:
    Population() = default;
    explicit Population(std::vector<Solution> solutions) noexcept;

    Fronts nondominated_sort() noexcept;
};

template<typename Solution, std::size_t N>
auto Population<Solution, N>::nondominated_sort() noexcept -> Fronts
{
    Front first_front;
    SortingData sorting_data(*this);
    for (auto& p : *this) {
        auto& el = sorting_data.get_element_for_individual(p);
        for (auto& q : *this) {
            if (p.dominates(q)) {
                el.add_dominated_individual(q);
            } else if (q.dominates(p)) {
                el.dominated_by_counter++;
            }
        }
        if (el.dominated_by_counter == 0) {
            el.rank = 1;
            first_front.add(p);
        }
    }

    Fronts fronts;
    fronts.add(std::move(first_front));
    for (std::size_t i = 0; true; i++) {
        Front next_front;
        for (const auto& front_el : fronts[i]) {
            auto& el_p = sorting_data.get_element_for_individual(
                front_el.get_individual());
            for (auto* q : el_p.get_dominated_individuals()) {
                auto& el_q = sorting_data.get_element_for_individual(*q);
                if (--el_q.dominated_by_counter == 0) {
                    el_q.rank = i + 1;
                    next_front.add(*q);
                }
            }
        }
        if (next_front.empty()) {
            break;
        } else {
            fronts.add(std::move(next_front));
        }
    }

    // Set rank for each individual in the population.
    std::size_t rank = 1;
    for (const auto& front : fronts) {
        for (const auto& el : front) { el.get_individual().set_rank(rank); }
        rank++;
    }

    return fronts;
}

template<typename Solution, std::size_t N>
Population<Solution, N>::Front::Front() noexcept
{
    constexpr static std::size_t reserve_size = 32;
    this->reserve(reserve_size);
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::Front::add(Individual& individual) noexcept
{
    SHO_ASSERT(!contains(individual),
               "Front already contains provided individual.");
    elements.emplace_back(individual);
}

template<typename Solution, std::size_t N>
bool Population<Solution, N>::Front::contains(
    const Individual& individual) const noexcept
{
    return std::find_if(elements.begin(),
                        elements.end(),
                        [&individual](const Element& el) {
                            return &individual == &el.get_individual();
                        }) != elements.end();
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::Front::crowding_sort(
    const FitnessDomain& domain) noexcept
{
    calculate_crowding_distances(domain);
    std::sort(elements.begin(),
              elements.end(),
              [](const Element& lhs, const Element& rhs) {
                  return lhs.get_crowding_distance() >
                         rhs.get_crowding_distance();
              });
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::Front::reserve(std::size_t reserve_size) noexcept
{
    elements.reserve(reserve_size);
}

template<typename Solution, std::size_t N>
bool Population<Solution, N>::Front::empty() const noexcept
{
    return elements.empty();
}

template<typename Solution, std::size_t N>
std::size_t Population<Solution, N>::Front::size() const noexcept
{
    return elements.size();
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::Front::operator[](
    std::size_t index) const noexcept -> const Element&
{
    SHO_ASSERT(index < elements.size(), "Index out of range.");
    return elements[index];
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::Front::begin() const noexcept -> ConstIterator
{
    return elements.begin();
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::Front::end() const noexcept -> ConstIterator
{
    return elements.end();
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::Front::calculate_crowding_distances(
    const FitnessDomain& domain) noexcept
{
    SHO_ASSERT(!elements.empty(),
               "Cannot calculate crowding distances on an empty front.");

    for (auto& element : elements) { element.set_crowding_distance(0.0); }

    for (std::size_t m = 0; m < N; m++) {
        std::sort(elements.begin(),
                  elements.end(),
                  [m](const Element& lhs, const Element& rhs) {
                      return lhs.get_individual().get_fitness()[m] <
                             rhs.get_individual().get_fitness()[m];
                  });

        constexpr static auto inf = std::numeric_limits<double>::infinity();
        elements.front().set_crowding_distance(inf);
        elements.back().set_crowding_distance(inf);

        for (std::size_t i = 1; i < this->size() - 1; i++) {
            double cd = elements[i].get_crowding_distance();
            if (!std::isinf(cd)) {
                const auto& next = elements[i + 1].get_individual();
                const auto& prev = elements[i - 1].get_individual();

                SHO_ASSERT(domain[m].contains(next.get_fitness()[m]),
                           "Fitness out of domain range.");
                SHO_ASSERT(domain[m].contains(prev.get_fitness()[m]),
                           "Fitness out of domain range.");

                cd += std::abs(next.get_fitness()[m] - prev.get_fitness()[m]) /
                      domain[m].size();
            }
            elements[i].set_crowding_distance(cd);
        }
    }
}

template<typename Solution, std::size_t N>
Population<Solution, N>::Front::Element::Element(
    Individual& individual) noexcept
        : individual(&individual)
{}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::Front::Element::get_individual() const noexcept
    -> Individual&
{
    SHO_ASSERT(individual, "Individual should never be nullptr.");
    return *individual;
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::Front::Element::set_crowding_distance(
    double crowding_distance) noexcept
{
    this->crowding_distance = crowding_distance;
}

template<typename Solution, std::size_t N>
double
Population<Solution, N>::Front::Element::get_crowding_distance() const noexcept
{
    SHO_ASSERT(crowding_distance, "Crowding has not yet been set.");
    return *crowding_distance;
}

template<typename Solution, std::size_t N>
Population<Solution, N>::Fronts::Fronts() noexcept
{
    constexpr static std::size_t reserve_size = 32;
    this->reserve(reserve_size);
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::Fronts::add(Front front) noexcept
{
    SHO_ASSERT(!contains_any_from_front(front),
               "Each element in each fron must be unique.");
    this->push_back(std::move(front));
}

template<typename Solution, std::size_t N>
bool Population<Solution, N>::Fronts::contains_any_from_front(
    const Front& front) const noexcept
{
    for (const auto& f : *this) {
        for (const auto& el : f) {
            if (front.contains(el.get_individual())) { return true; }
        }
    }
    return false;
}

template<typename Solution, std::size_t N>
Population<Solution, N>::SortingData::Element::Element(
    const Individual& individual) noexcept
        : individual(&individual), dominated_by_counter(0), rank(0)
{
    constexpr static std::size_t reserve_size = 32;
    dominated_individuals.reserve(reserve_size);
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::SortingData::Element::add_dominated_individual(
    Individual& i) noexcept
{
    SHO_ASSERT(std::find(dominated_individuals.begin(),
                         dominated_individuals.end(),
                         &i) == dominated_individuals.end(),
               "Dominated individuals must be unique.");
    dominated_individuals.push_back(&i);
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::SortingData::Element::get_individual()
    const noexcept -> const Individual&
{
    SHO_ASSERT(individual, "Individual should never be nullptr.");
    return *individual;
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::SortingData::Element::get_dominated_individuals()
    const noexcept -> const std::vector<Individual*>&
{
    return dominated_individuals;
}

template<typename Solution, std::size_t N>
Population<Solution, N>::SortingData::SortingData(
    const Population& population) noexcept
{
    elements.reserve(population.size());
    for (const auto& individual : population) {
        elements.emplace_back(individual);
    }
}

template<typename Solution, std::size_t N>
void Population<Solution, N>::SortingData::add(Element element) noexcept
{
    SHO_ASSERT(std::find_if(elements.begin(),
                            elements.end(),
                            [&element](const Element& el) {
                                return el.get_individual() ==
                                       element.get_individual();
                            }) == elements.end(),
               "Multiple elements for the same individual.");
    elements.push_back(std::move(element));
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::SortingData::get_element_for_individual(
    const Individual& i) noexcept -> Element&
{
    const auto it = std::find_if(
        elements.begin(), elements.end(), [&i](const Element& e) noexcept {
            return &i == &e.get_individual();
        });
    SHO_ASSERT(it != elements.end(), "No element for provided individual.");
    return *it;
}

template<typename Solution, std::size_t N>
auto Population<Solution, N>::SortingData::get_elements() const noexcept
    -> const std::vector<Element>&
{
    return elements;
}

template<typename Solution, std::size_t N>
Population<Solution, N>::Population(std::vector<Solution> solutions) noexcept
{
    this->reserve(solutions.size());
    for (auto& solution : solutions) {
        this->emplace_back(std::move(solution));
    }
}

}

#endif
