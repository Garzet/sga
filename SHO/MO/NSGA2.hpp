#ifndef SHO_MO_NSGA2_HPP
#define SHO_MO_NSGA2_HPP

#include "SHO/Random.hpp"
#include "SHO/Assert.hpp"
#include "SHO/State.hpp"
#include "SHO/Mutation.hpp"
#include "SHO/Crossover.hpp"
#include "SHO/TerminationCriterion.hpp"

#include "SHO/MO/Evaluator.hpp"
#include "SHO/MO/Individual.hpp"
#include "SHO/MO/Population.hpp"

#include <unordered_set>

namespace SHO::MO {

template<typename Solution, std::size_t N>
class NSGA2 {
  public:
    using State         = SHO::State;
    using Crossover     = SHO::Crossover<Solution>;
    using Mutation      = SHO::Mutation<Solution>;
    using Evaluator     = SHO::MO::Evaluator<Solution, N>;
    using Individual    = SHO::MO::Individual<Solution, N>;
    using Population    = SHO::MO::Population<Solution, N>;
    using Fitness       = SHO::MO::Fitness<N>;
    using FitnessDomain = SHO::MO::FitnessDomain<N>;

    class Selection {
      public:
        using PopulationConstIterator = typename Population::const_iterator;

        struct Result {
            const Solution& first_parent;
            const Solution& second_parent;
            PopulationConstIterator individual_to_eliminate;
        };

      public:
        virtual ~Selection() = default;

        virtual Result select(const Population& population) = 0;
    };

    struct Result {
        Population final_population;
        const TerminationCriterion& met_criterion;
        State state;
    };

    using InspectionFunction =
        std::function<void(const Population& population, const State& state)>;

    class TournamentSelection : public Selection {
      public:
        using typename Selection::Result;

      private:
        std::size_t population_size;
        std::size_t tournament_size;
        std::vector<std::size_t> index_vector;
        std::vector<std::size_t> selected_indices;

      public:
        TournamentSelection(std::size_t population_size,
                            std::size_t tournament_size) noexcept;

        Result select(const Population& population) override;
    };

    class RouletteWheel : public Selection {
      public:
        using typename Selection::Result;
        using typename Selection::PopulationConstIterator;

      private:
        std::size_t population_size;
        std::vector<unsigned> weights;

      public:
        RouletteWheel(std::size_t population_size) noexcept;

        Result select(const Population& population) override;
    };

  private:
    std::unique_ptr<Evaluator> evaluator;
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria;
    Random::ProbabilityVector<std::unique_ptr<Selection>> selections;
    Random::ProbabilityVector<std::unique_ptr<Crossover>> crossovers;
    Random::ProbabilityVector<std::unique_ptr<Mutation>> mutations;
    FitnessDomain fitness_domain;
    double mutation_probability;

  public:
    NSGA2(
        std::unique_ptr<Evaluator> evaluator,
        std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
        Random::ProbabilityVector<std::unique_ptr<Selection>> selections,
        Random::ProbabilityVector<std::unique_ptr<Crossover>> crossovers,
        Random::ProbabilityVector<std::unique_ptr<Mutation>> mutations,
        FitnessDomain fitness_domain,
        double mutation_probability)
    noexcept;

    Result run(
        std::vector<Solution> initial_solutions,
        InspectionFunction inspection_function =
            [](const Population& /*population*/,
               const State& /*state*/) noexcept {});

  private:
    const TerminationCriterion*
    check_termination_critera(const State& state) const;

    void update_algorithm_state(
        State& state,
        std::unordered_set<Fitness>& pareto_fitnesses,
        const std::chrono::high_resolution_clock::time_point& start_timepoint,
        const Population& population) noexcept;

    void populate_pareto_fitnesses(std::vector<Fitness>& pareto_fitnesses,
                                   const Population& population) const noexcept;
};

template<typename Solution, std::size_t N>
NSGA2<Solution, N>::NSGA2(
    std::unique_ptr<Evaluator> evaluator,
    std::vector<std::unique_ptr<TerminationCriterion>> termination_criteria,
    Random::ProbabilityVector<std::unique_ptr<Selection>> selections,
    Random::ProbabilityVector<std::unique_ptr<Crossover>> crossovers,
    Random::ProbabilityVector<std::unique_ptr<Mutation>> mutations,
    FitnessDomain fitness_domain,
    double mutation_probability) noexcept
        : evaluator(std::move(evaluator)),
          termination_criteria(std::move(termination_criteria)),
          selections(std::move(selections)),
          crossovers(std::move(crossovers)),
          mutations(std::move(mutations)),
          fitness_domain(std::move(fitness_domain)),
          mutation_probability(mutation_probability)
{
    SHO_ASSERT(this->evaluator, "Evaluator may not be nullptr.");
}

template<typename Solution, std::size_t N>
auto NSGA2<Solution, N>::run(std::vector<Solution> initial_solutions,
                             InspectionFunction inspection_function) -> Result
{
    SHO_ASSERT(!initial_solutions.empty(),
               "At least one initial solution must be provided.");
    const auto population_size = initial_solutions.size();

    Population population(std::move(initial_solutions));
    population.reserve(population_size * 2);

    std::unordered_set<Fitness> pareto_fitnesses;
    pareto_fitnesses.reserve(population_size);

    // Evaluate and sort initial population so that the best solutions are at
    // the front of the population.
    for (auto& individual : population) {
        individual.set_fitness(evaluator->evaluate(individual.solution));
    }
    population.nondominated_sort();

    State state{0,
                0,
                population.size(), // Entire initial population is evaluated.
                std::chrono::seconds(0)};

    inspection_function(population, state);

    Population next_population;
    next_population.reserve(population_size);

    const auto start_timepoint = std::chrono::high_resolution_clock::now();
    const TerminationCriterion* met_criterion = nullptr;
    while (!(met_criterion = check_termination_critera(state))) {
        for (std::size_t i = 0; i < population_size; i++) {
            Selection& selection = *selections.get_random_element();
            auto selected        = selection.select(population);

            Crossover& crossover = *crossovers.get_random_element();
            auto child =
                crossover.mate(selected.first_parent, selected.second_parent);

            std::bernoulli_distribution mut_dist(mutation_probability);
            if (mut_dist(Random::get_generator())) {
                Mutation& mutation = *mutations.get_random_element();
                mutation.mutate(child);
            }

            Individual new_individual{std::move(child)};
            new_individual.set_fitness(
                evaluator->evaluate(new_individual.solution));

            next_population.push_back(std::move(new_individual));
        }

        std::move(next_population.begin(),
                  next_population.end(),
                  std::back_inserter(population));
        next_population.clear();

        auto fronts = population.nondominated_sort();
        for (auto& front : fronts) {
            if (front.size() <= population_size - next_population.size()) {
                for (const auto& front_el : front) {
                    next_population.push_back(
                        std::move(front_el.get_individual()));
                }
                if (population_size == next_population.size()) { break; }
            } else {
                front.crowding_sort(fitness_domain);
                for (const auto& front_el : front) {
                    next_population.push_back(
                        std::move(front_el.get_individual()));
                    if (next_population.size() >= population_size) { break; }
                }
                break;
            }
        }

        std::swap(population, next_population);
        next_population.clear();

        update_algorithm_state(
            state, pareto_fitnesses, start_timepoint, population);
        inspection_function(population, state);
    }

    return {std::move(population), *met_criterion, std::move(state)};
}

template<typename Solution, std::size_t N>
const TerminationCriterion*
NSGA2<Solution, N>::check_termination_critera(const State& state) const
{
    for (auto& termination_criterion : termination_criteria) {
        if (termination_criterion->is_met(state)) {
            return termination_criterion.get();
        }
    }
    return nullptr;
}

template<typename Solution, std::size_t N>
void NSGA2<Solution, N>::update_algorithm_state(
    State& state,
    std::unordered_set<Fitness>& pareto_fitnesses,
    const std::chrono::high_resolution_clock::time_point& start_timepoint,
    const Population& population) noexcept
{
    state.iteration++;

    bool new_pareto_fitness_found = false;
    for (const auto& individual : population) {
        if (individual.is_pareto_individual() &&
            !pareto_fitnesses.count(individual.get_fitness())) {
            new_pareto_fitness_found = true;
            break;
        }
    }
    if (new_pareto_fitness_found) {
        pareto_fitnesses.clear();
        for (const auto& individual : population) {
            if (individual.is_pareto_individual()) {
                pareto_fitnesses.insert(individual.get_fitness());
            }
        }
        state.n_iterations_without_improvement = 0;
    } else {
        state.n_iterations_without_improvement++;
    }

    state.n_evaluations += population.size();
    state.running_time =
        std::chrono::high_resolution_clock::now() - start_timepoint;
}

template<typename Solution, std::size_t N>
void NSGA2<Solution, N>::populate_pareto_fitnesses(
    std::vector<Fitness>& pareto_fitnesses,
    const Population& population) const noexcept
{
    pareto_fitnesses.clear();
    for (const auto& individual : population) {
        if (individual.is_pareto_individal()) {
            pareto_fitnesses.push_back(individual.get_fitness());
        }
    }
}

template<typename Solution, std::size_t N>
NSGA2<Solution, N>::TournamentSelection::TournamentSelection(
    std::size_t population_size, std::size_t tournament_size) noexcept
        : population_size(population_size), tournament_size(tournament_size)
{
    [[maybe_unused]] constexpr static std::size_t min_tournament_size = 3;
    SHO_ASSERT_FMT(tournament_size >= min_tournament_size,
                   "Tournament size must be at least {} (provided: {}).",
                   min_tournament_size,
                   tournament_size);
    SHO_ASSERT_FMT(tournament_size <= population_size,
                   "Tournament size ({}) must be lower or equal to "
                   "population size ({}).",
                   tournament_size,
                   population_size);

    // Generate index vector.
    index_vector.reserve(population_size);
    for (std::size_t i = 0; i < population_size; i++) {
        index_vector.push_back(i);
    }

    selected_indices.reserve(tournament_size);
}

template<typename Solution, std::size_t N>
auto NSGA2<Solution, N>::TournamentSelection::select(
    const Population& population) -> Result
{
    SHO_ASSERT_FMT(population.size() == population_size,
                   "Tournament selection initialized with population size "
                   "equal to {}, but population of size {} was provided.",
                   population_size,
                   population.size());

    std::sample(index_vector.begin(),
                index_vector.end(),
                std::back_inserter(selected_indices),
                tournament_size,
                Random::get_generator());

    const auto comparator = [&population](std::size_t lhs,
                                          std::size_t rhs) noexcept {
        return population[lhs].get_rank() < population[rhs].get_rank();
    };

    // Find the best individual in the tournament.
    auto it = std::max_element(
        selected_indices.begin(), selected_indices.end(), comparator);
    std::size_t best_index = *it;
    selected_indices.erase(it);

    // Find the second best individual in the tournament.
    it = std::max_element(
        selected_indices.begin(), selected_indices.end(), comparator);
    std::size_t second_best_index = *it;

    // Find the worst individual in the tournament.
    it = std::min_element(
        selected_indices.begin(), selected_indices.end(), comparator);
    std::size_t worst_index = *it;

    selected_indices.clear(); // Clear selected indices for next iteration.

    return {population[best_index].solution,
            population[second_best_index].solution,
            population.begin() + worst_index};
}

template<typename Solution, std::size_t N>
NSGA2<Solution, N>::RouletteWheel::RouletteWheel(
    std::size_t population_size) noexcept
        : population_size(population_size)
{
    SHO_ASSERT(population_size > 0, "Population size may not be zero.");
    weights.reserve(population_size);
}

template<typename Solution, std::size_t N>
auto NSGA2<Solution, N>::RouletteWheel::select(const Population& population)
    -> Result
{
    SHO_ASSERT_FMT(population_size == population.size(),
                   "Roulette wheel selection expects population of size {}, "
                   "but population of size {} was provided.",
                   population_size,
                   population.size());

    weights.clear();
    for (const auto& individual : population) {
        unsigned fitness = individual.get_rank();
        weights.push_back(fitness);
    }

    std::discrete_distribution<std::size_t> parent_dist(weights.begin(),
                                                        weights.end());

    const Solution& first_parent =
        population[parent_dist(Random::get_generator())].solution;
    const Solution& second_parent =
        population[parent_dist(Random::get_generator())].solution;

    // Inverse weights to select individual to eliminate.
    for (auto& weight : weights) { weight = 1.0 / weight; }
    std::discrete_distribution<std::size_t> elim_dist(weights.begin(),
                                                      weights.end());
    PopulationConstIterator individual_to_eliminate =
        population.begin() + elim_dist(Random::get_generator());

    return {first_parent, second_parent, individual_to_eliminate};
}

}

#endif
