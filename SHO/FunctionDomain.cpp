#include "SHO/FunctionDomain.hpp"

namespace SHO {

std::ostream& operator<<(std::ostream& os, const RealFunctionDomain& d)
{
    return os << '{' << d.min << d.max << '}';
}

}
