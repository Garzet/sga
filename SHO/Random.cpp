#include "SHO/Random.hpp"

#include <optional>

namespace SHO::Random {

std::mt19937& get_generator() noexcept
{
    // To ensure that experiments that run in parallel do not use the same
    // random generator (for seeding reasons), thread local storage is used.
    // This makes this function thread safe and also enables setting the seed
    // per experiment even if multiple experiments run in parallel.
    thread_local std::random_device rng_device;
    thread_local std::mt19937 rng_generator(rng_device());
    return rng_generator;
}

}
